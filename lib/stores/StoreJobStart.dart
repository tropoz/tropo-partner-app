import 'package:shared_preferences/shared_preferences.dart';

class StoreJobStart{
  static String start_job = "start_job";

  static Future  getStartJob() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(start_job);
  }
  static Future  setStartJob() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    try{
      return await prefs.setBool("start_job",true);
    }catch(e){
      throw e;
    }
  }

  static Future removeJob() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    try{
      return await prefs.remove(start_job);
    }catch(e){
      throw e;
    }
  }
}