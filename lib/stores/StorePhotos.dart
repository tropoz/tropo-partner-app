import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import 'dart:convert';


class StorePhotos {
  static const pre_photos = "pre_photos";

  static const post_photos = "post_photos";

  static Future saveImageOfPostToPreferences(List value,key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> valueSaved = value.map((e) => json.encode(e)).toList();
    return prefs.setStringList("post_photos_$key",valueSaved);
  }

  static Future getPhotosOfPostPrefernces(key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    if(prefs.getStringList("post_photos_$key") != null){
      return prefs.getStringList("post_photos_$key").map((e)=>json.decode(e)).toList();
    }
    return [];
  } 

  static Future deletePhotosOfPostPrefernces(key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.remove("post_photos_$key"); 
  }

  // setup for folder and files 

  static Future saveImageOfPreToPreferences(List value,key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> valueSaved = value.map((e) => json.encode(e)).toList();
    return prefs.setStringList("pre_photos_$key",valueSaved);
  }

  static Future getPhotosOfPrePrefernces(key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    if(prefs.getStringList("pre_photos_$key") != null){
      return prefs.getStringList("pre_photos_$key").map((e)=>json.decode(e)).toList();
    }
    return [];
  } 

  static Future deletePhotosOfPrePrefernces(key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.remove("pre_photos_$key"); 
  }

}