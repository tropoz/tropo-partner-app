import 'dart:async';

import 'package:flutter/material.dart';


import 'package:shared_preferences/shared_preferences.dart';

import 'screens/authentication/home.dart';
import 'screens/authentication/login.dart';

// import 'package:provider/provider.dart';


class AuthOrHome extends StatefulWidget {

  @override
  _AuthOrHomeState createState() => _AuthOrHomeState();
}

class _AuthOrHomeState extends State<AuthOrHome> {

  // check if user if auth or not if isAuth then auth else not
  bool isLoggedIn = false;

  bool isLoading = true;


  void authenticated() async {

    try{
      var prefs = await SharedPreferences.getInstance();
      var isAuth = prefs.getString("token");

      if(isAuth != null){
          Future.delayed(Duration(seconds:3),(){
            setState(() {
              isLoading = false;
              isLoggedIn = true;
            });
          });
      }else{
        Future.delayed(Duration(seconds:3),(){
          setState(() {
            isLoggedIn = false;
            isLoading = false;
          });
        });
      }
    }catch(e){
      print("Error $e");
      throw e;
    }



  }

  @override
  void initState() {

    authenticated();
    
    super.initState();
  }


  
  //states goes here
  @override
  Widget build(BuildContext context)  {
    return isLoading ? Scaffold(
      body:Container(
        color:Colors.white,
        child:Center(
          child: Image.asset("assets/tropo_logo.png",fit: BoxFit.contain,height: 250,),
        )
      )
    ) : (isLoggedIn ? Home() : Login());
  }
}