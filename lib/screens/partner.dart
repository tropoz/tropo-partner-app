import 'package:flutter/material.dart';
import 'package:troppo_mobile/widgets/AppHeader.dart';

import '../widgets/BottomNavigation.dart';

class PartnerCare extends StatefulWidget {

  @override
  _PartnerCareState createState() => _PartnerCareState();
}

class _PartnerCareState extends State<PartnerCare> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppHeader("Partner Care",false),
      bottomNavigationBar: BottomNavigation(4),
      body:Center(
        child:Text("Partner care")
      )
    );
  }
}