import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
// import 'package:hexcolor/hexcolor.dart';

import '../../widgets/NoDataFound.dart';

// ignore: must_be_immutable
class KitChemical extends StatefulWidget {
  List data = [];
  KitChemical(this.data);
  @override
  _KitTab createState() => _KitTab();
}

class _KitTab extends State<KitChemical> {

  @override
  Widget build(BuildContext context) {
    return widget.data.length > 0 ? Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children:[
        Expanded(
          child:ListView.builder(
            itemCount:widget.data.length,
            itemBuilder:(_,index){
            return Card(
            elevation: 2,
            child: ListTile(
              leading: Icon(Icons.invert_colors,size:40,color: Colors.blue,),
              title:Text("${widget.data[index]['name']}"),
              subtitle:Row(
                children: [
                  Text("Qty:",style: TextStyle(color: Colors.red,fontWeight:FontWeight.bold)),
                  Text(":-${widget.data[index]['quantity']} lte",style: TextStyle(color:HexColor("#a9a8a9"))),

                ],
              ),
            ));
          }
        ))
      ],
    ) : NoDataFound();
  }
}