import 'package:flutter/material.dart';
import 'package:troppo_mobile/widgets/NoDataFound.dart';

// ignore: must_be_immutable
class KitDetail extends StatefulWidget {
  Map kit;
  KitDetail(this.kit);
  @override
  _KitTab createState() => _KitTab();
}

class _KitTab extends State<KitDetail> {

  @override
  initState(){
    super.initState();

  }
  @override
  Widget build(BuildContext context) {

    return widget.kit.length > 0 ? SingleChildScrollView(
      child:  Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children:[
        Card(
        elevation: 2,
        child: ListTile(
          contentPadding: EdgeInsets.all(10),
          title:Text(widget.kit['name'] ?? "",style:TextStyle(fontSize:20,fontWeight: FontWeight.w500)),
          subtitle:Padding(
            padding: EdgeInsets.only(top:12),
            child: Text(widget.kit['detail'] ?? ""),
          )
        ),
      ),
      SizedBox(height:80),
      if(widget.kit['kit_item'] != null) ...(widget.kit['kit_item']).map((value){
          return Card(
            elevation: 2,
            child: ListTile(
          leading: Icon(Icons.build,size:40,),
          contentPadding: EdgeInsets.all(10),
          title:Text(value['item_type']),
            subtitle:Padding(
              padding: EdgeInsets.only(top:12),
              child: Text(value['item_detail']),
            )
        ),
          );
      }).toList(),
      ],
    )) : NoDataFound();
  }
}