import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:troppo_mobile/widgets/NoDataFound.dart';

// ignore: must_be_immutable
class KitRefill extends StatefulWidget {
  List data = [];
  KitRefill(this.data);
  @override
  _KitTab createState() => _KitTab();
}

class _KitTab extends State<KitRefill> {


  @override
  Widget build(BuildContext context) {
    return (widget.data.length > 0) ? (Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children:[
          Expanded(child:ListView.builder(
            itemCount: widget.data.length,
            itemBuilder:(_,index){
              return Card(
        elevation: 2,
        child: ListTile(
          leading: Icon(Icons.invert_colors,size:40,color: Colors.blue,),
          title:Text("${widget.data[index]['chemical']}"),
          subtitle:Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children:[
              Row(
                children: [
                  Text("Refilled Qty",style: TextStyle(color: Colors.red,fontSize:15,fontWeight:FontWeight.bold),),
                  Text(":${widget.data[index]['available']} ltr.",style: TextStyle(color:HexColor("#a9a8a9"),fontWeight: FontWeight.bold),)
                ],
              ),

              SizedBox(height: 8,),
              Text("${widget.data[index]['date_time']} by manager",style: TextStyle(color: HexColor("#6057a5"),fontWeight: FontWeight.bold,),textAlign: TextAlign.start,),
            ]
          ),
          ),
        );
            })),
      ],
    )) : NoDataFound();
  }
}