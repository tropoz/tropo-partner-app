import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import 'package:troppo_mobile/providers/Auth.dart';

import 'package:troppo_mobile/widgets/NetWorkIssue.dart';
import 'package:troppo_mobile/widgets/ServerErrorWidget.dart';
// kit details
import 'kit_detail.dart';
// kit chemicals
import 'kit_chemical.dart';
// kit refills
import 'kit_refill.dart';

import '../../widgets/Loader.dart';

import 'package:http/http.dart' as http;

import '../../constants.dart';

class MyKit extends StatefulWidget {
  @override
  _MyKitState createState() => _MyKitState();
}

class _MyKitState extends State<MyKit> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  void _showSnackBar(value) {
    final snackBar = SnackBar(
      backgroundColor: Colors.red,
      content: Text(value),
      duration: Duration(seconds: 10),
      // behavior: SnackBarBehavior.floating,
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  Map kit = {};

  List chemicals = [];

  List log = [];

  bool isLoading = true;

  bool isError = false;

  bool isNetwork = true;

  void isConnectionMethod() async {
    print("Is connection");
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      setState(() {
        isNetwork = true;
      });
      getKits();
    } else {
      setState(() {
        isNetwork = false;
      });
    }
  }

  Future getKits() async {
    Future.delayed(Duration.zero, () async {
      try {
        var token = await Provider.of<Auth>(context, listen: false).getToken();
        http.Response res = await http
            .get("$api_url/api/cleaner/assigned_kit_list", headers: {
          "Accept": "application/json",
          "Authorization": "Bearer $token"
        });
        var resData = jsonDecode(res.body)['data'];
        print(jsonDecode(res.body));
        if (resData.length > 0) {
          resData = resData[0];
          setState(() {
            kit = resData['kit'] ?? {};
            chemicals = resData['chemical'] ?? [];
            log = resData['log'] ?? [];
            isLoading = false;
          });
        } else {
          setState(() {
            kit = {};
            chemicals = [];
            log = [];
            isLoading = false;
          });
        }
      } catch (e) {
        // _showSnackBar("Something went wrong!please try again later");
        setState(() {
          isError = true;
        });
        throw e;
      }
    });
  }

  @override
  void initState() {
    isConnectionMethod();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(120.0),
          child: AppBar(
            title: Text("My kits"),
            backgroundColor: Theme.of(context).primaryColor,
            bottom: TabBar(
              indicatorColor: Colors.white,
              tabs: [
                Tab(child: Text("Detail", style: TextStyle(fontSize: 15))),
                Tab(child: Text("Chemicals", style: TextStyle(fontSize: 15))),
                Tab(child: Text("Refill logs", style: TextStyle(fontSize: 15))),
              ],
            ),
          ),
        ),
        body: isNetwork
            ? (isLoading
                ? Loader()
                : ServerErrorWidget(
                    isError: isError,
                    contain: Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 10, horizontal: 8),
                      child: TabBarView(children: [
                        KitDetail(kit),
                        KitChemical(chemicals),
                        KitRefill(log),
                      ]),
                    ),
                  ))
            : NetWorkIssue(isConnectionMethod),
      ),
    );
  }
}
