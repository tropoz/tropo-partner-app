import 'package:flutter/material.dart';

import '../../widgets/AppButton.dart';

class CreatePassword extends StatefulWidget {
  CreatePassword({Key key}) : super(key: key);

  @override
  _CreatePasswordState createState() => _CreatePasswordState();
}

class _CreatePasswordState extends State<CreatePassword> {

  TextEditingController text_new_password = TextEditingController();

  TextEditingController text_confirm_password = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  bool onSubmit = false;

  bool isVisiblePassword = false;
  @override
  Widget build(BuildContext context) {
    final focus = FocusNode();
    final new_password = TextFormField(
      controller: text_new_password,
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (v){
        FocusScope.of(context).requestFocus(focus);
      },
      validator: (value) {
        if (value.trim().isEmpty) {
          return "New password is required";
        } else if (value.trim().length < 6) {
          return "Password must be 6 character";
        }
        return null;
      },
      obscureText: isVisiblePassword ? false : true,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          suffixIcon: IconButton(icon: !isVisiblePassword ? Icon(Icons.visibility_off) : Icon(Icons.visibility), onPressed: (){
                setState(() {
                  isVisiblePassword =!isVisiblePassword;
                });
          }),
          hintText: "New Password",
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0))),
    );
    final confirm_password = TextFormField(
      focusNode: focus,
      controller: text_confirm_password,
      validator: (value) {
        if (value.trim().isEmpty) {
          return "Confirm password required";
        }else if(value != text_new_password.text){
          return "Password is n't matching";
        }
        return null;
      },
      obscureText: true,
      onFieldSubmitted: (v){
        FocusScope.of(context).requestFocus(FocusNode());
        
      },
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Confirm Password",
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0))),
    );
  
    return Scaffold(
      body: SingleChildScrollView(
          child: Padding(
        padding: EdgeInsets.only(top: 60, left: 10, right: 10, bottom: 10),
        child: Form(
          // ignore: deprecated_member_use
          autovalidate: onSubmit,
          key: _formKey,
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                  children: [
                      Icon(Icons.vpn_key,size: 50,),
                      Text("Create Password",style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    )
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height / 4.5,
              ),
              Align(
                child: Text(
                  "Enter New Password :",
                  textAlign: TextAlign.start,
                ),
                alignment: Alignment.centerLeft,
              ),
              SizedBox(
                height: 4,
              ),
              new_password,
              SizedBox(
                height: 20,
              ),
              Align(
                child: Text(
                  "Confirm Password :",
                  textAlign: TextAlign.start,
                ),
                alignment: Alignment.centerLeft,
              ),
              SizedBox(
                height: 4,
              ),
              confirm_password,
              SizedBox(height: 20),
              AppButton(text:"Next",method : (){
                setState(() {
                  onSubmit = true;
                });
                if(_formKey.currentState.validate()){
                  FocusScope.of(context).unfocus();
                  Navigator.pushNamedAndRemoveUntil(context, "/login",(_)=>false);
                }
              }),
            ],
          ),
        ),
      )),
    );
  }
}
