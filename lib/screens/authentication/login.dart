import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:connectivity/connectivity.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:progress_indicator_button/progress_button.dart';
import 'package:troppo_mobile/providers/Auth.dart';
import 'package:troppo_mobile/widgets/NetWorkIssue.dart';

import 'package:provider/provider.dart';

import '../../widgets/SocketExceptionWidget.dart';

class Login extends StatefulWidget {
  Login({Key key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  bool onSubmit = false;

  bool isNetwork = true;

  TextEditingController mobile = TextEditingController();

  TextEditingController password = TextEditingController();

  void isConnectionMethod() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      setState(() {
        isNetwork = true;
      });
    } else {
      setState(() {
        isNetwork = false;
      });
    }
  }

  @override
  initState() {
    isConnectionMethod();
    super.initState();
  }

  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  void _showSnackBar(value) {
    final snackBar = SnackBar(
      backgroundColor: Colors.red,
      content: Text(value),
      duration: Duration(seconds: 3),
      // behavior: SnackBarBehavior.floating,
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    final authenticate = Provider.of<Auth>(context, listen: false);
    final phoneField = TextFormField(
      controller: mobile,
      validator: (value) {
        if (value.trim().isEmpty) {
          return "Required phone no";
        } else if (value.trim().length < 10 || value.trim().length > 10) {
          return "Enter valid phone no";
        }
        return null;
      },
      obscureText: false,
      style: style,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Mobile no",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );
    final passwordField = TextFormField(
      controller: password,
      obscureText: true,
      style: style,
      validator: (value) {
        if (value.isEmpty) {
          return "Required password field";
        }

        return null;
      },
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Password",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );
    final loginButon = ProgressButton(
      borderRadius: BorderRadius.circular(5.0),
      color: Theme.of(context).primaryColor,
      onPressed: (AnimationController controller) async {
        setState(() {
          onSubmit = true;
        });
        if (_formKey.currentState.validate()) {
          controller.forward();

          try {
            var loginStatus = await authenticate.authenticate(
                {"mobile": mobile.text, "password": password.text});
            print(jsonDecode(loginStatus.body));

            controller.reverse();

            if (loginStatus.statusCode == 422) {
              EasyLoading.dismiss();
              setState(() {
                mobile.text = "";
                password.text = "";
                onSubmit = false;
              });

              _showSnackBar(jsonDecode(loginStatus.body)['message']);
            } else {
              EasyLoading.dismiss();
              Navigator.pushReplacementNamed(context, "/home");
            }
          } catch (e) {
            controller.reverse();

            // if connection error occur then show dialock box
            SocketError(context);

            setState(() {
              mobile.text = "";
              password.text = "";
            });

            throw e;
          }

          // Navigator.pushNamed(context, "/");
        }
      },
      child: Text("Login",
          textAlign: TextAlign.center,
          style:
              style.copyWith(color: Colors.white, fontWeight: FontWeight.bold)),
    );

    return isNetwork
        ? Scaffold(
            key: _scaffoldKey,
            backgroundColor: Colors.white,
            body: SingleChildScrollView(
                child: Center(
              child: Container(
                child: Padding(
                  padding: const EdgeInsets.all(36.0),
                  child: Form(
                      // ignore: deprecated_member_use
                      autovalidate: onSubmit ? true : false,
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Image.asset(
                            "assets/tropo_logo.png",
                            fit: BoxFit.contain,
                            height: 250,
                          ),
                          SizedBox(height: 45.0),
                          phoneField,
                          SizedBox(height: 25.0),
                          passwordField,
                          SizedBox(
                            height: 10.0,
                          ),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                FlatButton(
                                    onPressed: () {
                                      Navigator.pushNamed(
                                          context, "/forget/password");
                                    },
                                    child: Text(
                                      "Forgot Password",
                                      style:
                                          TextStyle(color: HexColor("#5155b1")),
                                    )),
                              ]),
                          SizedBox(
                            height: 10.0,
                          ),
                          loginButon,
                        ],
                      )),
                ),
              ),
            )),
          )
        : NetWorkIssue(isConnectionMethod);
  }
}
