import 'dart:async';

import 'dart:typed_data';

// import 'package:audioplayers/audio_cache.dart';
// import 'package:audioplayers/audioplayers.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import 'package:google_map_polyline/google_map_polyline.dart';

import 'package:flutter_phone_direct_caller/flutter_phone_direct_caller.dart';

import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:troppo_mobile/constants.dart';
import 'package:troppo_mobile/screens/notifications/alarm.dart';
import 'package:troppo_mobile/widgets/ServerErrorWidget.dart';
import 'package:url_launcher/url_launcher.dart';

import 'dart:ui' as ui;

import '../../widgets/SocketExceptionWidget.dart';

import 'package:http/http.dart' as http;

import '../../widgets/BottomNavigation.dart';

import 'package:flutter/material.dart';

import 'package:hexcolor/hexcolor.dart';

import '../../widgets/Loader.dart';

import '../../widgets/AppDrawer.dart';

import 'package:connectivity/connectivity.dart';

import 'package:google_maps_flutter/google_maps_flutter.dart';


import 'package:location/location.dart';


import 'package:badges/badges.dart';

import 'package:provider/provider.dart';

import 'package:animated_widgets/animated_widgets.dart';


import '../../providers/Auth.dart';

import '../../widgets/NetWorkIssue.dart';

// import 'package:flutter_spinkit/flutter_spinkit.dart';





class Home extends StatefulWidget {

  @override
  _HomeState createState() => _HomeState();
}

  Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) async {
      print("onBackgroundMessage: $message");
      //_showBigPictureNotification(message);
      return _HomeState()._showNotification();
    }
class _HomeState extends State<Home> {
  bool isNetwork = true;

  bool isLoading = true;

  int dutyMode = 0;


Completer<GoogleMapController> _controller = Completer();


  FlutterLocalNotificationsPlugin fltrNotification;


  Map user_data = {};

  var markerImage;

  bool isNotify = false;

  bool isSwitch = false;

  double lat;

  bool isError = false;

  double long;



  // FirebaseMessaging _firebaseMessaging = FirebaseMessaging();


// ignore: missing_return

GoogleMapPolyline googleMapPolyline = GoogleMapPolyline(apiKey: "AIzaSyBi-bkxbmIz0PEda-0f9nDHLZBtMUPCOOI");

List<LatLng> routeCoords =[];

FirebaseMessaging _firebaseMessaging = FirebaseMessaging();


// FirebaseMessaging messaging = FirebaseMessaging.instance;

Location location = new Location();


int counterNotify = 0;

final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
void _showSnackBar(value) {
  final snackBar = SnackBar(
    backgroundColor: Colors.red,
    content: Text(value),
    duration: Duration(seconds: 3),
    // behavior: SnackBarBehavior.floating,
  );
  _scaffoldKey.currentState.showSnackBar(snackBar);
}


Future _showNotification() async {
  FlutterLocalNotificationsPlugin fltrNotification;
  var androidInitilize = new AndroidInitializationSettings('@mipmap/ic_launcher');
  var iOSinitilize = new IOSInitializationSettings();
  var initilizationsSettings = new InitializationSettings(android:androidInitilize,iOS:iOSinitilize);
  fltrNotification = new FlutterLocalNotificationsPlugin();
  fltrNotification.initialize(initilizationsSettings,
      onSelectNotification: notificationSelected);
  var androidDetails = new AndroidNotificationDetails(
        "Channel ID", "Desi programmer", "This is my channel",
        importance: Importance.max);
  var iSODetails = new IOSNotificationDetails();
  var generalNotificationDetails =
        new NotificationDetails(android:androidDetails,iOS:iSODetails);
  await fltrNotification.show(0, "title"," body", generalNotificationDetails,payload: 'Default_Sound');
  
}

Future changeJobStatus() async {
  setState(() {
    if(dutyMode == 1){
      dutyMode = 0;
    }else{
      dutyMode = 1;
    }
  });
  try{
    final String token =  await Provider.of<Auth>(context,listen: false).getToken();
    http.Response a = await http.post("$api_url/api/cleaner/change_working_status",headers: {
      "Authorization":"Bearer $token"
    },body: {
      "user_id":user_data['data']['id'].toString(),
      "status":dutyMode.toString()
    });
    if(a.statusCode == 200){
      await Provider.of<Auth>(context,listen: false).getUserApi();
    }
  }catch(e){
    print("Error $e");
  }
}
final Set<Polyline> polyline = {};
  getSomepoints() async {
    print("google map polyline");
    routeCoords = await googleMapPolyline.getCoordinatesWithLocation(origin: LatLng(lat,long), destination:LatLng(28.021211,73.298480), mode: RouteMode.driving);
    if(routeCoords.length > 0){
      setState(() {
          polyline.add(Polyline(
            jointType: JointType.round,
            consumeTapEvents: true,
            polylineId: PolylineId("route1"),
            visible: true,
            points:routeCoords,
            width: 4,
            color:Colors.blue,
        ),);
      });
    }
  }
  Future fetchUserInfo(){
    print("User info");

    
    
    Future.delayed(Duration.zero,(){
        Provider.of<Auth>(context,listen: false).getUserApi().then((value){
          setState(() {
            user_data = Provider.of<Auth>(context,listen: false).getUser;
          });
          print("User data with detail $user_data");

        
          if(user_data['status_code'].toString() == "401"){
           
            Provider.of<Auth>(context,listen: false).logout().then((v){
                Navigator.pushNamedAndRemoveUntil(context, "/login",(_)=>false);
            });

            return;
          }

          //set user detail here
          setState(() {
            user_data = user_data;
            isLoading = false;
            dutyMode = int.parse(user_data['data']['working_status'].toString());
          });
        }).catchError((e){
          setState(() {
            isLoading = false;
            isError = true;
          });
          print(e);
          // SocketError(context);
        });
    });
  }
  void isConnectionMethod() async {
    print("Is connection");
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile || connectivityResult == ConnectivityResult.wifi) {
        print("Net work is working");
        setState(() {
          isNetwork = true;
        });

        fetchUserInfo();
    } else{
        setState(() {
          isNetwork = false;
        });
    }
  }

  
  _getLocation() async {
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }
    
    location.getLocation().then((value){
      setState(() {
        lat = value.latitude;
        long = value.longitude;
      });
    });
    print("Called");
  }

  Future getNotification(String f_token) async{
    final String token = await Provider.of<Auth>(context,listen: false).getToken(); 
    final user = Provider.of<Auth>(context,listen: false).getUser;
    print(token);
    try{
      http.Response res = await http.post('$api_url/api/cleaner/firebase_token',headers: {
        "Authorization":"Bearer $token"
      },body:{
        "token":f_token,
      });
      print(res.body);
    }catch(e){
      print(e);
    }
  }

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(), targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png)).buffer.asUint8List();
  }

  getImage() async{
    final http.Response response = await http.get("https://tropo.in/logo.png");

    setState(() {
      markerImage = BitmapDescriptor.fromBytes(response.bodyBytes);
      print(markerImage);
    });
  }


  

  @override 
  void initState(){
    // getImage();
    _removeNotify();

    var androidInitilize = new AndroidInitializationSettings('@mipmap/ic_launcher');
  var iOSinitilize = new IOSInitializationSettings();
  var initilizationsSettings = new InitializationSettings(android:androidInitilize,iOS:iOSinitilize);
  fltrNotification = new FlutterLocalNotificationsPlugin();
  fltrNotification.initialize(initilizationsSettings,
      onSelectNotification: notificationSelected);

      
    _getLocation();
    _firebaseMessaging.configure(
      onMessage: (msg){
        print("Msg found");
        Navigator.pushNamed(context,"/start/alarm");
        setState(() {
          counterNotify++;
          isNotify = true;
        });
        Future.delayed(Duration(seconds: 4),(){
          setState(() {
            isNotify = false;
          });
        });
      },
      onResume: (msg){
        print("Resume $msg");
      },
      onLaunch: (msg){
        print("Lanunch");
      },
      onBackgroundMessage:myBackgroundMessageHandler
    );
  
    
    isConnectionMethod();
    Future.delayed(Duration.zero,(){
      _firebaseMessaging.getToken().then((token) async {
        print(token);
        if(user_data != {}){
          try{
            final String authToken = await Provider.of<Auth>(context,listen: false).getToken();
            http.Response res  = await http.post("$api_url/api/cleaner/firebase_token",headers: {
              "Authorization":"Bearer $authToken"
            },body: {
              "user_id":user_data['id'].toString(),
              "token":token.toString(),
            });
          }catch(e){
            throw e;
          }
        }
      });
      showModalBottomSheet<void>(
        context: context,
        isDismissible: false,
        builder: (BuildContext context) {
          return Container(
            height: MediaQuery.of(context).size.height - 100,
            child: Stack(
            
              children: [
                Column(
                  children: <Widget>[
                    
                    Container(
                      color: HexColor("#5155B1"),
                      child:Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                          
                         Padding(
                           padding: EdgeInsets.all(10),
                           child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment:  MainAxisAlignment.spaceBetween,
                            children:[
                              Text("Service Name",style: TextStyle(color: Colors.white,fontSize:20,fontWeight: FontWeight.bold),),
                              Text("Pakage Name",style: TextStyle(color: Colors.white,fontSize:15),),
                              Padding(padding: EdgeInsets.only(top:10),
                              child:Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text("Vechile Name",style: TextStyle(color: Colors.white,fontSize:12),),
                                  Text("Vechile Other Detail",style: TextStyle(color: Colors.white,fontSize:12),),
                                ],
                              ))
                            ]
                          ),
                         ),     
                         Padding(
                           padding: EdgeInsets.only(top:50),
                           child: Column(
                           children:[
                             
                             Text("4 KM",style: TextStyle(color: Colors.white),),
                             Image.network("https://cdn.pixabay.com/photo/2012/04/12/23/48/car-30990__340.png",height:100,width:100)
                           ]
                          ),
                         ),
                      ],
                    ),),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: Column(
                        children:[
                          ListTile(
                            leading:Icon(Icons.account_circle),
                            title:Text("Customer Name"),
                          ),
                          ListTile(
                            leading:Icon(Icons.phone),
                            title:Text("+91-6350095470",style: TextStyle(color: Colors.red),),
                          ),
                          Divider(),
                          ListTile(
                            leading:Icon(Icons.location_on),
                            title:Text("1612 Stockton St San Francisco,CA 94133"),
                          )
                        ]
                      ),
                    )
                  ],
                ),
                Align(
                  alignment: AlignmentDirectional.topEnd,
                  child: Transform.translate(
                    offset: Offset(0, -20),
                    child: FlatButton(
                      child: Icon(Icons.done, size: 30),
                      shape: CircleBorder(),
                      textColor: Colors.white,
                      padding:EdgeInsets.all(10),
                      color: HexColor("#58E994"),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                )),
              ]
            ),
            );
          },
      );

      // check whatever time it's for morning task
      // begin code here
      var morningTimeHour = int.parse(DateFormat('h').format(DateTime.now()));
      var morningTimeMeridian = DateFormat('a').format(DateTime.now());
      if(morningTimeHour <= 12 && morningTimeMeridian == "AM"){
          showDialog(
            context: context,
            child:AlertDialog(
            insetPadding: EdgeInsets.all(10),
            contentPadding: EdgeInsets.all(0.0),
            content: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                width: double.infinity,
                height: 200,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Good Morning!",style:TextStyle(color:Colors.white,fontSize: 20)),
                    SizedBox(height:15),
                  ],
                ),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/home.jpg"),
                    fit: BoxFit.cover,
                  ),
                )
              ),
              Padding(padding:EdgeInsets.all(10),child:Text(
                      """
Location is disabled on this device. Please enable it and try again.
                      """)),
                ],
              ),
              actions: [
                FlatButton(
                  child: const Text("CONTINUE",style: TextStyle(color:Colors.blue),),
                  onPressed: (){
                    Navigator.of(context, rootNavigator: true).pop('dialog');
                  },
                ),
              ],
            ),
        );
      }

      // end here 
    

    });
    
    super.initState();
  }

   @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    print('state = $state');
  }


  _removeNotify(){
     Timer(Duration(seconds:2), (){
        if(mounted){
            setState(() {
              isNotify = false;
            });
        }
      });
  }



  @override
  Widget build(BuildContext context) {

    final user = Provider.of<Auth>(context).getUser;
    String name = user != null ? user['data']['name'] : "";
    location.onLocationChanged.listen((LocationData currentLocation) {
        lat = currentLocation.latitude;
        long = currentLocation.longitude;
    });

    BitmapDescriptor customIcon;

// make sure to initialize before map loading
    BitmapDescriptor.fromAssetImage(ImageConfiguration(size: Size(12, 12)),
            'assets/tropo_logo.png')
        .then((d) {
      customIcon = d;
    });
    return Scaffold(
      key: _scaffoldKey,
      drawer: AppDrawer(),
      bottomNavigationBar: BottomNavigation(0),
      appBar:PreferredSize(
          preferredSize: Size.fromHeight(70.0),
          child:AppBar(
            title:Text("Welcome, $name"),
            actions: [
              
              Padding(
                padding:EdgeInsets.only(right:20,top: 5),child:GestureDetector(

                onTap: (){
                  Navigator.pushNamed(context, "/notifications");
                },
                child: Tooltip(
                    message: "2",
                    child: ShakeAnimatedWidget(
                    enabled: isNotify,
                    duration: Duration(milliseconds: 200),
                    shakeAngle: Rotation.deg(z: 40),
                    curve: Curves.linear,
                    child:Badge(
                      badgeColor:Colors.redAccent,
                      position: BadgePosition.topEnd(top: 0, end: -5),
                      badgeContent: Text("$counterNotify"),
                      child: Icon(Icons.notification_important,color: Colors.white,size:26,),
                    ),
                  ),
                ),
              )),
  
            ],
            backgroundColor: Theme.of(context).primaryColor,
          )
        ), 
        body: isNetwork ?  isLoading ? Loader() : ServerErrorWidget(
                isError: isError,
                contain: Stack(
                children: [
                lat != null && long != null ? GoogleMap(
                  trafficEnabled: true,
                  markers: Set<Marker>.of(
                  <Marker>[
                    Marker(
                      markerId: MarkerId("1"),
                      position: LatLng(lat, long),
                      icon: BitmapDescriptor.defaultMarker,
                      infoWindow: const InfoWindow(
                        title: 'You',
                      ),
                    ),
                    // Marker(
                    //   markerId: MarkerId("2"),
                    //   position: LatLng(28.021211,73.298480),
                    //   icon: customIcon,
                    //   infoWindow: InfoWindow(
                    //     title: 'Pick area',
                    //     snippet:"Call Me :6350095470",
                    //     onTap: () async {
                    //       // make a call 
                    //       const number = '6350095470'; //set the number here
                    //       bool res = await FlutterPhoneDirectCaller.callNumber(number);
                        
                    //     },
                    //   ),
                    // ),
                  ],
                ),
                myLocationButtonEnabled: true,
                myLocationEnabled: true,
                // trafficEnabled:true,
                compassEnabled:true,
                zoomGesturesEnabled: true,
                tiltGesturesEnabled: false,
                onMapCreated: (GoogleMapController controller){
                      _controller.complete(controller);
                      print("Init google maps");
                      getSomepoints();
                     
                    },
                  initialCameraPosition:CameraPosition(
                  target: LatLng(lat,long),
                  zoom:20,
                  
                  ),
                  
                  mapType: MapType.normal,
                  // markers: _markers,
                  onCameraMove:(CameraPosition position){

                  },
                  // polylines:polyline
              ) : Text(""),
              Container(
                  color:dutyMode == 1 ? HexColor("#58E380") :  HexColor("#808080"),
                  padding: EdgeInsets.all(8),
                  height: MediaQuery.of(context).size.height / 11,
                  child:Center(child:Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      IconButton(icon: Icon(Icons.power_settings_new),onPressed:changeJobStatus,iconSize: 30,color: Colors.white,),
                      Text("${dutyMode == 1 ? "You are on duty" : "You are off duty" }",style: TextStyle(color: Colors.white,fontSize: 18,fontWeight: FontWeight.bold),),
                      
                    ],
                  ))
                ), 
            ],
          ),
        ) : NetWorkIssue(isConnectionMethod),
    );
  }

   Future notificationSelected(String payload) async {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          content: Text("Notification Clicked $payload"),
        ),
      );
    }
}