import 'package:flutter/material.dart';

import '../../widgets/AppHeader.dart';

class OTPSent extends StatefulWidget {
  OTPSent({Key key}) : super(key: key);

  @override
  _OTPSentState createState() => _OTPSentState();
}

class _OTPSentState extends State<OTPSent> {

  var mobile = TextEditingController();
  bool onSubmit = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  final _formKey = GlobalKey<FormState>();
  void _showSnackBar(value,[Color color]) {
    final snackBar = SnackBar(
      content: Text(value),
      duration: Duration(seconds: 3),
      backgroundColor: color,
      // behavior: SnackBarBehavior.floating,
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppHeader("Forgot Password"),
      body:Padding(
        padding: EdgeInsets.all(10),
        child:Form(
          key: _formKey,
          // ignore: deprecated_member_use
          autovalidate: onSubmit ? true : false,
          child:Column(
              children: [
                TextFormField(
                  
                  autofocus: true,
                  controller:mobile,
                  keyboardType: TextInputType.number,
                  validator: (value){
                    if(value.isEmpty){
                      return "Required otp number";
                    }

                    return null;
                  },
                  
                ),
                SizedBox(
                  height:20.0
                ),
                Material(
                elevation: 5.0,
                borderRadius: BorderRadius.circular(5.0),
                color: Theme.of(context).primaryColor,
                child: MaterialButton(
                  minWidth: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  onPressed: () async {
                    setState(() {
                      onSubmit = true;
                    });
                    if (_formKey.currentState.validate()){
                      FocusScope.of(context).requestFocus(FocusNode());

                      if(mobile.text == "12345"){
                        _showSnackBar("OTP is valid");
                        Future.delayed(Duration(seconds:2),(){
                          Navigator.pushReplacementNamed(context, "/create/password");
                        });
                      }else{
                        _showSnackBar("OTP is in-valid",Colors.red);
                      }
                    }
                  },
                  child: Text("ENTER OTP",
                    textAlign: TextAlign.center,style: TextStyle(color: Colors.white),),
                  ),
              )
            ],
          )
        ),
      )
    );
  }
}