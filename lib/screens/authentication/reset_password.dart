import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart';
import 'package:troppo_mobile/constants.dart';

import '../../widgets/AppHeader.dart';

class ResetPassword extends StatefulWidget {
  ResetPassword({Key key}) : super(key: key);

  @override
  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  var mobile = TextEditingController();
  bool onSubmit = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  Future sendOtp() async {
    setState(() {
      onSubmit = true;
    });
    if (!_formKey.currentState.validate()) {
      return;
    }
    FocusScope.of(context).requestFocus(FocusNode());
    EasyLoading.show(status: 'loading...');
    try {
      Response res = await post("$api_url/api/cleaner/forgot_password",
          body: {"mobile": mobile.text});
      EasyLoading.dismiss();
      final resBody = jsonDecode(res.body);
      if(res.statusCode == 404){
        _showSnackBar(resBody['message']);
        return;
      }
      print(jsonDecode(res.body));
    } catch (e) {
      throw e;
    }
  }

  final _formKey = GlobalKey<FormState>();
  void _showSnackBar(value,{Color color = Colors.red}) {
    final snackBar = SnackBar(
      content: Text(value),
      backgroundColor: color,
      duration: Duration(seconds: 3),
      // behavior: SnackBarBehavior.floating,
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppHeader("Reset Password"),
        body: Padding(
          padding: EdgeInsets.all(10),
          child: Form(
              key: _formKey,
              // ignore: deprecated_member_use
              autovalidate: onSubmit ? true : false,
              child: Column(
                children: [
                  TextFormField(
                    autofocus: true,
                    controller: mobile,
                    keyboardType: TextInputType.number,
                    validator: (value) {
                      if (value.isEmpty) {
                        return "Required mobile number";
                      } else if (value.trim().length < 10 ||
                          value.trim().length > 10) {
                        return "Please enter valid mobile no";
                      }

                      return null;
                    },
                  ),
                  SizedBox(height: 20.0),
                  Material(
                    elevation: 5.0,
                    borderRadius: BorderRadius.circular(5.0),
                    color: Theme.of(context).primaryColor,
                    child: MaterialButton(
                      minWidth: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      onPressed: sendOtp,
                      child: Text(
                        "SEND OTP",
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  )
                ],
              )),
        ));
  }
}
