import 'package:flutter/material.dart';

import 'package:qr_code_scanner/qr_code_scanner.dart';

class QRViewExample extends StatefulWidget {
  QRViewExample({Key key}) : super(key: key);

  @override
  _QRViewExampleState createState() => _QRViewExampleState();
}

class _QRViewExampleState extends State<QRViewExample> {
  GlobalKey _key = GlobalKey(debugLabel: 'QR');

  QRViewController _controller;

  int counter = 0;

  Barcode data;

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Stack(
        children:[
          QRView(
            key: _key, overlay: QrScannerOverlayShape(
            borderColor:Colors.red,
          ), onQRViewCreated:(QRViewController controller){
            this._controller = controller;
            controller.scannedDataStream.listen((scanData) {
              setState(() {
                data = scanData;
              });
              print(scanData.code);

              if(mounted){
                print("Mounnted");
                controller.pauseCamera();
                controller.dispose();
                Navigator.pop(context,{
                  "scanned":true,
                  "code":scanData.code,
                });
              }else{
                print("Didn't mount");
              }
            });
            // controller.flipCamera();
          }),
          Align(
            alignment: Alignment.topCenter,
            child:Container(
              margin:EdgeInsets.only(top:60),
              child: Text(
                "Scanner QR Code",
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
            )
          )
        ]
      )
    );
  }
}