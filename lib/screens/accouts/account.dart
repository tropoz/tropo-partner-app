import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

import '../../widgets/AppHeader.dart';

import '../../widgets/BottomNavigation.dart';


class Account extends StatefulWidget {
  Account({Key key}) : super(key: key);

  @override
  _AccountState createState() => _AccountState();
}

class _AccountState extends State<Account> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppHeader("Account Summary",false),
      bottomNavigationBar: BottomNavigation(1),
      body:Container(
        padding: EdgeInsets.all(5),
        child:Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children:[
            Card(
              elevation:2,
              child:ListTile(
                contentPadding: EdgeInsets.all(5),

                leading:Icon(Icons.money,size:50,color: HexColor("#5fea9f")),
                title:Text("Cash Collected"),
                subtitle:Text("Today Summary"),
                onTap: (){
                  Navigator.pushNamed(context, "/account/cash/collect");
                },
                trailing: Wrap(

                  children: [
                    Container(
                      margin:EdgeInsets.only(top:5),
                      child: Text("1000",style: TextStyle(fontSize:15),),
                    ),
                    Icon(Icons.keyboard_arrow_right,size: 30,)
                  ],
                ),
              )
            ),
            Card(
              elevation:2,
              child:ListTile(
                contentPadding: EdgeInsets.all(5),
                leading:Icon(Icons.money,size:50,color: HexColor('#cadc33'),),
                title:Text("Cash Collected"),
                subtitle:Text("Month Summary"),
                onTap: (){
                  Navigator.pushNamed(context, '/account/cash/month');
                },
                trailing: Wrap(

                  children: [
                    Container(
                      margin:EdgeInsets.only(top:5),
                      child: Text("1000",style: TextStyle(fontSize:15),),
                    ),
                    Icon(Icons.keyboard_arrow_right,size: 30,)
                  ],
                ),
              )
            ),
            Card(
              elevation:2,
              child:ListTile(
                contentPadding: EdgeInsets.all(5),

                leading:Icon(Icons.money,size:50,color: HexColor("#ffb343"),),
                title:Text("Cash Deposit"),
                subtitle:Text("Month Summary"),
                onTap: (){
                  Navigator.pushNamed(context, "/account/cash/deposit");
                },
                trailing: Wrap(
                  
                  children: [
                    Container(
                      margin:EdgeInsets.only(top:5),
                      child: Text("1000",style: TextStyle(fontSize:15),),
                    ),
                    Icon(Icons.keyboard_arrow_right,size: 30,)
                  ],
                ),
              )
            ),
            Card(
              elevation:2,
              child:ListTile(
                contentPadding: EdgeInsets.all(5),
                leading:Icon(Icons.money,size:50,color: HexColor("#fa6b6b"),),
                title:Text("Remuneration Receviced"),
                subtitle:Text("Month Summary"),
                onTap: (){
                  Navigator.pushNamed(context, "/account/cash/remuneration_receviced");
                },
                trailing: Wrap(

                  children: [
                    Container(
                      margin:EdgeInsets.only(top:5),
                      child: Text("1000",style: TextStyle(fontSize:15),),
                    ),
                    Icon(Icons.keyboard_arrow_right,size: 30,)
                  ],
                ),
              )
            )
          ]
        ),
      )
    );
  }
}