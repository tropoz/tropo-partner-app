import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import '../../widgets/AppHeader.dart';

class CashDeposit extends StatefulWidget {
  CashDeposit({Key key}) : super(key: key);

  @override
  _CashDepositState createState() => _CashDepositState();
}

class _CashDepositState extends State<CashDeposit> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppHeader("Cash collected"),
      body:Padding(
        padding: EdgeInsets.all(8),
        child: Column(
        crossAxisAlignment:CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(top:10),
            child:Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment:CrossAxisAlignment.start,
                  children: [
                    Text("Cash Deposit(Month Summary)"),
                    Text("Total:-1000/-",style: TextStyle(fontWeight:FontWeight.bold,color: HexColor("#767676")),)
                  ],
                ),
                Padding(
                  padding:EdgeInsets.only(right:12),
                  child:DropdownButton<String>(
                    value: "Jan",
                    items: <String>['Jan', 'Feb', 'Mar', 'Apr'].map((String value) {
                      return new DropdownMenuItem<String>(
                        value: value,
                        child: new Text(value),
                      );
                    }).toList(),
                    onChanged: (value) {
                      print(value);
                    },
                  )),
              ],
            ),
          ),
          Expanded(
            child:ListView.builder(
              itemCount: 3,
              itemBuilder:(create,index){
              return Card(
                elevation: 3,
                child:ListTile(
                contentPadding: EdgeInsets.all(9),
                title: Text("01-01-2020"),
                subtitle: Text("Reciver Name",style:TextStyle(color:Colors.red)),
                trailing: Text("550 /-",style: TextStyle(color: HexColor("#cadb30"),fontWeight: FontWeight.bold),),
            ));
          }))
        ],
      ),
      )
    );
  }
}