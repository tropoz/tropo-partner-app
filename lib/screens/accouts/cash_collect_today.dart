import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import '../../widgets/AppHeader.dart';

class CashCollect extends StatefulWidget {
  CashCollect({Key key}) : super(key: key);

  @override
  _CashCollectState createState() => _CashCollectState();
}

class _CashCollectState extends State<CashCollect> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppHeader("Cash collected"),
      body:Padding(
        padding: EdgeInsets.all(8),
        child: Column(
        crossAxisAlignment:CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(top:10),
            child:Column(
              crossAxisAlignment:CrossAxisAlignment.start,
              children: [
                 Text("Cash collected(Total Summary)"),
                 Text("Total:-1000/-",style: TextStyle(fontWeight:FontWeight.bold,color: HexColor("#767676")),)

              ],
            ),
          ),
          Expanded(
            child:ListView.builder(
              itemCount: 3,
              itemBuilder:(create,index){
              return Card(
                elevation: 3,
                child:ListTile(
                contentPadding: EdgeInsets.all(9),
                title: Text("7:00-10:00"),
                trailing: Text("550 /-",style: TextStyle(color: HexColor("#58e380"),fontWeight: FontWeight.bold),),
            ));
          }))
        ],
      ),
      )
    );
  }
}