import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import '../../widgets/AppHeader.dart';

class RemunationRecieve extends StatefulWidget {
  RemunationRecieve({Key key}) : super(key: key);

  @override
  _RemunationRecieveState createState() => _RemunationRecieveState();
}

class _RemunationRecieveState extends State<RemunationRecieve> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppHeader("Cash collected"),
      body:Padding(
        padding: EdgeInsets.all(8),
        child: Column(
        crossAxisAlignment:CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(top:10),
            child:Column(
              crossAxisAlignment:CrossAxisAlignment.start,
              children: [
                 Text("Remunation Recieve"),
              ],
            ),
          ),
          Expanded(
            child:ListView.builder(
              itemCount: 3,
              itemBuilder:(create,index){
              return Card(
                elevation: 3,
                child:ListTile(
                contentPadding: EdgeInsets.all(9),
                title: Text("01-01-2020"),
                subtitle: Text("Sender Name",style: TextStyle(color:HexColor("#6fc597")),),
              ));
          }))
        ],
      ),
      )
    );
  }
}