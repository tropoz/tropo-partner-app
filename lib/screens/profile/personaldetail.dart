import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:troppo_mobile/constants.dart';
import 'package:troppo_mobile/providers/Auth.dart';
import '../../widgets/AppHeader.dart';
import 'package:http/http.dart' as http;

class PersonalDetail extends StatefulWidget {
  PersonalDetail({Key key}) : super(key: key);

  @override
  _PersonalDetailState createState() => _PersonalDetailState();
}

class _PersonalDetailState extends State<PersonalDetail> {
  final _formKey = GlobalKey<FormState>();

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  bool onSubmit = false;

  File _image;
  var filename;

  final picker = ImagePicker();

  //static methods for update user data's
  static String _static_name;
  static String _static_mobile;
  static String _static_address;
  static String _static_email;



  TextEditingController _name;

  TextEditingController _mobile;

  TextEditingController _address;

  TextEditingController _email;


  // ends static methods

  void _showSnackBar(value,{Color color:Colors.red}) {
    final snackBar = SnackBar(
      content: Text(value),
      backgroundColor: color,
      duration: Duration(seconds: 3),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }


  var user_data;
  //cities variables here's
  List cities = [];
  String selected_city_name = "";
  int selected_city_id;
  List city_name = [];
  //states variables here's
  List states = [];
  String selected_state_name = "";
  int selected_state_id;
  List state_name = [];
  


  Future _submit(user_intance) async {
    setState(() {
      onSubmit = true;
    });
    if (_formKey.currentState.validate()) {
      // EasyLoading.show(status:"Loading");
      FocusScope.of(context).unfocus();

      EasyLoading.show(status: "Please wait..");
      var request = http.MultipartRequest(
          'POST', Uri.parse('$api_url/api/cleaner/personal_detail_update'));
      request.fields.addAll({
        'mobile': _mobile.text,
        "email": _email.text,
        "name":_name.text,
        "address":address.text,
        "state_id":selected_state_id.toString(),
        "city_id":selected_city_id.toString(),
        "user_id":user_intance['data']['id'].toString()
      });
      final token = await Provider.of<Auth>(context,listen: false).getToken();
      final headers = {
        "Authorization": "Bearer $token",
        "Accept": "application/json",
        "Content-Type": "multipart/form-data",
      };

      if (_image != null) {

        print(filename);
        var file = await http.MultipartFile.fromPath(
          'avatar',
          filename,
        );
        print(file);
        request.files.add(file);

      }
      request.headers.addAll(headers);
      try{
        http.StreamedResponse response = await request.send();
        print(response.statusCode);
        if (response.statusCode == 200) {
          await Provider.of<Auth>(context,listen: false).getUserApi();
          print(await response.stream.bytesToString());
          EasyLoading.dismiss();
          _showSnackBar("Profile Updated Successfully",color: Colors.green);
        } else {
          var errors = jsonDecode(await response.stream.bytesToString());
          print(errors);
          for(final v in errors['errors']['mobile']){
            EasyLoading.dismiss();
            _showSnackBar(v,color: Colors.red);
          }
        }
      }catch(e){
        print(e);
      }
    }
  }
  
  Future fetchCitiesAndState() async {
    try{
      final String token = await Provider.of<Auth>(context,listen:false).getToken();
      final user_detail = Provider.of<Auth>(context,listen: false).getUser;
      print(user_detail);
      setState(() {
        user_data = user_detail;
        selected_state_name = user_detail['data']['state'].toString();
        _static_name = user_detail['data']['name'];
        _static_mobile = user_detail['data']['mobile'];
        _static_address = user_detail['data']['address'];
        _static_email = user_detail['data']['email'];

        selected_state_id = int.parse(user_detail['data']['state_id'].toString());
        selected_city_name = user_detail['data']['city'].toString();
        selected_city_id = int.parse(user_detail['data']['city_id'].toString());
      });
      http.Response res = await http.get("$api_url/api/cleaner/cities",headers:{
          "Authorization":"Bearer $token"
      });
      final fetchData = jsonDecode(res.body);

      //cities data from api
      if(fetchData['data'] != null){
        fetchData['data']['cities'].forEach((key,value){
          setState(() {
            city_name.add(value);
            cities.add({
              "id":key,
              "city_name":value
            });
          });
          
        });
      }
      //states data from api
      if(fetchData['data'] != null){
          fetchData['data']['states'].forEach((key,value){
          setState(() {
            state_name.add(value);
            states.add({
              "id":key,
              "state_name":value
            });
          });
        });
      }
    }catch(e){
      throw e;
    }
  }
  // get images 
  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        filename = pickedFile.path;
      } else {
        print('No image selected.');
      }
    });
  }
  @override
  void initState() {
    fetchCitiesAndState();
    super.initState();
  }
  // ignore: non_constant_identifier_names
  var text_input_name = TextEditingController();

  var mobile_number = TextEditingController();

  var address = TextEditingController();

  @override
  Widget build(BuildContext context) {
    _name = TextEditingController(text:_static_name);
    _mobile = TextEditingController(text:_static_mobile);
    address = TextEditingController(text:_static_address);
    _email = TextEditingController(text:_static_email);


    return Scaffold(
      key: _scaffoldKey,
      appBar:AppHeader("Personal Detail"),
        body:SingleChildScrollView(
          child: Container(
          padding: EdgeInsets.all(7),
          child: Card(
            elevation:3,
            child:Column(
              children:[
                _image == null
                    ? Container(
                        width: 100,
                        height: 100,
                        margin: EdgeInsets.only(top: 30),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.blue,width: 3.0,),
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              
                                image: NetworkImage(
                                    '${user_data['data']['avatar']}'))))
                    : Container(
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.blue,width: 3.0,),
                          
                        ),
                        margin: EdgeInsets.only(top: 30),
                        height: 100,
                        width: 100,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(500.0),
                          child: Image.file(
                            _image,
                            fit: BoxFit.cover,
                          ),
                        )),
                  
                  Padding(
                    padding: EdgeInsets.all(10),
                    child: Form(
              // ignore: deprecated_member_use
              key:_formKey ,
              child: Column(
              children:[
                TextFormField(
                  controller: _name,

                  decoration: InputDecoration(
                    labelText: 'Name'
                  ),
                  onChanged: (value){
                    _static_name = value;
                  },
                  // ignore: missing_return
                  validator: (value){
                    if(value.isEmpty){
                      return "Please enter name";
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: _email,

                  decoration: InputDecoration(
                    labelText: 'Email'
                  ),
                  onChanged: (value){
                    _static_email = value;
                  },
                  // ignore: missing_return
                  validator: (value){
                    if(value.isEmpty){
                      return "Please enter name";
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: _mobile,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    labelText: 'Mobile Number'
                  ),
                  onChanged: (value){
                    _static_mobile = value;
                  },
                  validator: (value){
                    if(value.isEmpty){
                      return "Please enter mobile no";
                    }
                    return null;
                  }
                ),
                DropdownButton<String>(
                  isExpanded: true,
                  value: selected_state_name,
                  items: state_name.toSet().toList()
                      .map((value) {
                    return new DropdownMenuItem<String>(
                      value: value,
                      child: new Text(value),
                    );
                  }).toList(),
                  onChanged: (value) {
                    //choosen states name
                    setState(() {
                      selected_state_name = value;
                    });

                    // state id 
                    states.forEach((element) {
                        if(element['state_name'].toString().contains(value)){
                         setState(() {
                            selected_state_id = int.parse(element['id'].toString());
                         });
                        }
                    });
                  },
                ),
                DropdownButton<String>(
                  isExpanded: true,
                  value: selected_city_name,
                  items: city_name.toSet().toList()
                      .map((value) {
                    return new DropdownMenuItem<String>(
                      value: value,
                      child: new Text(value),
                    );
                  }).toList(),
                  onChanged: (value) {
                    //choosen states name
                    setState(() {
                      selected_city_name = value;
                    });

                    // state id 
                    cities.forEach((element) {
                        if(element['city_name'].toString().contains(value)){
                         setState(() {
                            selected_city_id = int.parse(element['id'].toString());
                         });
                        }
                    });
                  },
                ),
                TextFormField(
                  controller: address,
                  decoration: InputDecoration(
                    labelText: 'Address'
                  ),
                  onChanged: (value){
                    _static_address = value;
                  },
                  validator: (value){
                    if(value.isEmpty){
                      return "Please enter address";
                    }
                    return null;
                  }
                ),
                SizedBox(height:17),
                  Material(
                  elevation: 5.0,
                  borderRadius: BorderRadius.circular(5.0),
                  color: Theme.of(context).primaryColor,
                  child: MaterialButton(
                  minWidth: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                  onPressed: () {
                      _submit(user_data);
                    },
                      child: Text("Submit",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold)),
                      ),
                    )
                  ],
                ),
                )
              )

              ]
            )
          ),
        ),
        
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: getImage,
          tooltip: 'Pick Image',
          child: Icon(Icons.add_a_photo),
        )
    );
  }
}