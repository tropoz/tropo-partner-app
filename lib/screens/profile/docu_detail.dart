import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:http/http.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:troppo_mobile/providers/Auth.dart';
import '../../constants.dart';
import '../../widgets/AppHeader.dart';

class DocumentDetail extends StatefulWidget {
  DocumentDetail({Key key}) : super(key: key);

  @override
  _DocumentDetailState createState() => _DocumentDetailState();
}

class _DocumentDetailState extends State<DocumentDetail> {
  final _formKey = GlobalKey<FormState>();

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  bool onSubmit = false;
  File aadhar_card;
  File driving_lic;
  File pan_card;
  File health_card;

  String aadhar_card_url = "";
  String driving_lic_url = "";
  String pan_card_url = "";
  String health_card_url = "";


  final picker = ImagePicker();

    Future<void> _cameraOptions(String file_url) {
    return showDialog(
            context: context,
            child:AlertDialog(
            insetPadding: EdgeInsets.all(10),
            contentPadding: EdgeInsets.all(0.0),
            content: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                width: double.infinity,
                height: 200,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: NetworkImage(file_url),
                    fit: BoxFit.fill,
                  ),
                )
              ),
                ],
              ),
            ),
        );
  }


  void _showSnackBar(value,{Color color:Colors.red}) {
    final snackBar = SnackBar(
      content: Text(value),
      backgroundColor: color,
      duration: Duration(seconds: 3),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  void upload_documents() async {
    if(aadhar_card_url != "" || driving_lic_url != "" || pan_card_url != "" || health_card_url != ""){
      final String token = await Provider.of<Auth>(context,listen: false).getToken();
      final  user_detail = Provider.of<Auth>(context,listen: false).getUser;
      final headers = {
          "Authorization": "Bearer $token",
          "Accept": "application/json",
          "Content-Type": "multipart/form-data",
      };
      EasyLoading.show(status: "Loading...");
      var request = MultipartRequest(
            'POST', Uri.parse('$api_url/api/cleaner/update_document'));
      request.fields.addAll({
        'user_id': user_detail['data']['id'].toString(),
      });
      // aadhar card file
      if(aadhar_card_url != ""){
        var file_aadhar = await MultipartFile.fromPath(
            'aadhar',
            aadhar_card_url,
          );
        request.files.add(file_aadhar);
      }
      // pan card file

      if(pan_card_url != ""){
        var file_pan = await MultipartFile.fromPath(
            'pan',
            pan_card_url,
          );
        request.files.add(file_pan);
      }
      // driving licence
      if(driving_lic_url != ""){
        var file_driving_licence = await MultipartFile.fromPath(
            'driving',
            driving_lic_url,
          );
      request.files.add(file_driving_licence);
      }


      // health card file upload
      if(health_card_url != ""){
        var file_health = await MultipartFile.fromPath(
            'health',
            health_card_url,
          );
        request.files.add(file_health);
      }

      request.headers.addAll(headers);

      try{
          StreamedResponse response = await request.send();
          print(response.statusCode);
          if (response.statusCode == 200) {
            await Provider.of<Auth>(context,listen: false).getUserApi();
            print(await response.stream.bytesToString());
            EasyLoading.dismiss();
            _showSnackBar("Document Uploaded successfully",color: Colors.green);
          } else {
            var errors = jsonDecode(await response.stream.bytesToString());
            print(errors);
          }
        }catch(e){
          print(e);
        }
    }else{
      print("No file selected yet!");
    }
    //
  }
  // ignore: non_constant_identifier_names
  var text_input_name = TextEditingController();
  @override
  Widget build(BuildContext context) {
    final user_detail = Provider.of<Auth>(context).getUser;
    print(user_detail);
    return Scaffold(
      key: _scaffoldKey,
      appBar:AppHeader("Document Detail"),
        body:SingleChildScrollView(
          child: Container(
          padding: EdgeInsets.all(7),
          child: Card(
            elevation:3,
            child:Column(
            children:[
              Padding(
                padding: EdgeInsets.all(10),
                child: Form(
                  // ignore: deprecated_member_use
                  autovalidate: onSubmit ? true : false,
                // ignore: deprecated_member_use
                  key:_formKey ,
                  child: Column(
                  children:[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children:[
                        
                        Text("Aadhar Card :"),
                        Row(
                          children: [
                            IconButton(
                              icon: Icon(Icons.visibility),
                              color: HexColor("#5155B1"),
                              iconSize: 25,
                              onPressed: () async {
                                _cameraOptions(user_detail['data']['aadhar']);
                              },
                            ),
                            IconButton(
                              icon: Icon(Icons.upload_file),
                              color: HexColor("#5155B1"),
                              iconSize: 25,
                              onPressed: () async {
                                final pickedFile = await picker.getImage(source: ImageSource.gallery);
                                setState((){
                                  if(pickedFile != null){
                                    aadhar_card_url = pickedFile.path;
                                  }
                                });
                              },
                            )
                          ],
                        ),
                        
                      ]
                    ),
                    SizedBox(height:15,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children:[
                        Text("Driving licence :"),
                        Row(
                          children: [
                            IconButton(
                              icon: Icon(Icons.visibility),
                              color: HexColor("#5155B1"),
                              iconSize: 25,
                              onPressed: () {
                                _cameraOptions(user_detail['data']['driving']);
                              },
                            ),
                            IconButton(
                              icon: Icon(Icons.upload_file),
                              color: HexColor("#5155B1"),
                              iconSize: 25,
                              onPressed: () async {
                                final pickedFile = await picker.getImage(source: ImageSource.gallery);
                                setState((){
                                  if(pickedFile != null){
                                    driving_lic_url = pickedFile.path;
                                  }
                                });
                              },
                            ),
                            
                          ],
                        )
                      ]
                    ),
                    SizedBox(height:15,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children:[
                        Text("Pan Card :"),
                        Row(
                          children: [
                            IconButton(
                              icon: Icon(Icons.visibility),
                              color: HexColor("#5155B1"),
                              iconSize: 25,
                              onPressed: () {
                                _cameraOptions(user_detail['data']['pan']);
                              },
                            ),
                            IconButton(
                              icon: Icon(Icons.upload_file),
                              color: HexColor("#5155B1"),
                              iconSize: 25,
                              onPressed: () async {
                                final pickedFile = await picker.getImage(source: ImageSource.gallery);
                                setState((){
                                  if(pickedFile != null){
                                    pan_card_url = pickedFile.path;
                                  }
                                });
                              },
                            ),
                            
                          ],
                        )
                      ]
                    ),
                    SizedBox(height:15,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children:[
                        Text("Health Card :"),
                        Row(
                          children: [
                            IconButton(
                              icon: Icon(Icons.visibility),
                              color: HexColor("#5155B1"),
                              iconSize: 25,
                              onPressed: () {
                                _cameraOptions(user_detail['data']['health']);
                              },
                            ),
                            IconButton(
                              icon: Icon(Icons.upload_file),
                              color: HexColor("#5155B1"),
                              iconSize: 25,
                              onPressed: () async {
                                final pickedFile = await picker.getImage(source: ImageSource.gallery);
                                setState((){
                                  if(pickedFile != null){
                                    health_card_url = pickedFile.path;
                                  }
                                });
                              },
                            ),
                            
                          ],
                        )
                      ]
                    ),
                    SizedBox(height:17),
                      Material(
                      elevation: 5.0,
                      borderRadius: BorderRadius.circular(5.0),
                      color: Theme.of(context).primaryColor,
                      child: MaterialButton(
                      minWidth: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      onPressed:upload_documents,
                          child: Text("Submit",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white, fontWeight: FontWeight.bold)),
                          ),
                        )
                      ],
                    ),
                    )
                  )
                ]
              )
            ),
          ),
        )
    );
  }
}