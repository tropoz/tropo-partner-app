import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
class Profile extends StatelessWidget {
  final List personal = [{"title":"Personal Detail","leading":Icons.mood,"route":"/profile/detail"},{"title":"Change Password","leading":Icons.mood,"route":"/reset/password"},{"title":"Document Detail","leading":Icons.content_copy,"route":"/profile/document"},{"title":"Bank Detail","leading":Icons.account_balance,"route":"/profile/bank/detail"},{"title":"Refer & Earn","leading":Icons.fast_forward,"route":"/profile/refer_earn"},{"title":"Settings","leading":Icons.mood,"route":"/profile/settings"},{"title":"Legal Information","leading":Icons.content_copy,"route":"/profile/legal_info"},{"title":"Privacy Policy","leading":Icons.home,"route":"/profile/privacy_policy"}];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:PreferredSize(
          preferredSize: Size.fromHeight(70),
          child:AppBar(
            title:Text("Profile"),
            backgroundColor: Theme.of(context).primaryColor,
          ),
        ),
        body:ListView.builder(
          itemCount: personal.length,
          itemBuilder:(create,index){
            return Column(
              children: [
                Ink(
                  color:(index == 2 || index == 5) ? HexColor("#f1f0f1") :Colors.white,
                  child:Padding(
                    padding: EdgeInsets.only(left:10),
                    child: ListTile(
                      contentPadding:EdgeInsets.all(6),
                      leading:Icon(personal[index]['leading'],size: 30,),
                      title:Text(personal[index]['title']),
                      onTap: (){
                       
                        Navigator.pushNamed(context, personal[index]['route']);
                      },
                    ),
                  ),
                ),
                Divider(),

              ],
            );
          }),
    );
  }
}