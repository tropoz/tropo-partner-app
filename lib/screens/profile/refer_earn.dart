import 'package:flutter/material.dart';
import '../../widgets/AppHeader.dart';


class ReferEarn extends StatefulWidget{
  ReferEarn({Key key}) : super(key: key);

  @override
  _ReferEarn createState() => _ReferEarn();
}
// ignore: must_be_immutable
class _ReferEarn extends State<ReferEarn> {
  final _formKey = GlobalKey<FormState>();

  bool onSubmit = false;

  // ignore: non_constant_identifier_names
  var bank_name = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppHeader("Refer a Friend"),
      body:SingleChildScrollView(
          child: Container(
          padding: EdgeInsets.all(7),
          child: Card(
            elevation:3,
            child:Column(
            children:[
              Padding(
                padding: EdgeInsets.all(10),
                child: Form(
                  // ignore: deprecated_member_use
                  autovalidate: onSubmit ? true : false,
                // ignore: deprecated_member_use
                  key:_formKey ,
                  child: Column(
                  children:[
                    TextFormField(
                      controller: bank_name,
                      decoration: InputDecoration(
                        labelText: 'Name'
                      ),
                      // ignore: missing_return
                      validator: (value){
                        if(value.isEmpty){
                          return "Please enter bank name";
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        labelText: 'A/c number'
                      ),
                      validator: (value){
                        if(value.isEmpty){
                          return "Please enter a/c number";
                        }
                        return null;
                      }
                    ),
                    TextFormField(
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        labelText: 'IFSC Code'
                      ),
                      validator: (value){
                        if(value.isEmpty){
                          return "Please enter IFSC code";
                        }
                        return null;
                      }
                    ),
                    TextFormField(
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        labelText: 'Branch address'
                      ),
                      validator: (value){
                        if(value.isEmpty){
                          return "Please enter branch address";
                        }
                        return null;
                      }
                    ),
                    SizedBox(height:17),
                      Material(
                      elevation: 5.0,
                      borderRadius: BorderRadius.circular(5.0),
                      color: Theme.of(context).primaryColor,
                      child: MaterialButton(
                      minWidth: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      onPressed: () {
                        setState(() {
                          onSubmit = true;
                        });
                        if(_formKey.currentState.validate()){
                            //clean input after submit form
                            print(bank_name);
                            FocusScope.of(context).requestFocus(FocusNode());

                        }
                      },
                          child: Text("Submit",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white, fontWeight: FontWeight.bold)),
                          ),
                        )
                      ],
                    ),
                    )
                  )
                ]
              )
            ),
          ),
      )
      
    );
  }
}