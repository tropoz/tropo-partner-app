import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:troppo_mobile/constants.dart';
import 'package:troppo_mobile/providers/Auth.dart';
import '../../widgets/AppHeader.dart';


class BankDetail extends StatefulWidget{
  BankDetail({Key key}) : super(key: key);

  @override
  _BankDetail createState() => _BankDetail();
}
// ignore: must_be_immutable
class _BankDetail extends State<BankDetail> {
  final _formKey = GlobalKey<FormState>();

  bool onSubmit = false;

  static String _static_bank_name;
  static String _static_bank_ac;
  static String _static_bank_ifsc_code;
  static String _static_bank_address;

  TextEditingController bank_name;

  TextEditingController bank_ac;

  TextEditingController bank_ifsc_code;

  TextEditingController bank_address;

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();


  Map user_detail;

   void _showSnackBar(value,{Color color:Colors.green}) {
    final snackBar = SnackBar(
      content: Text(value),
      backgroundColor: color,
      duration: Duration(seconds: 3),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }


  @override
  void initState() {
    Future.delayed(Duration.zero,(){
        final user_detail_get = Provider.of<Auth>(context,listen: false).getUser;
        print(user_detail_get);
        setState(() {
          user_detail = user_detail_get;
        });
        _static_bank_name = user_detail['data']['bank'];
        _static_bank_ac = user_detail['data']['bank_ac'];
        _static_bank_ifsc_code = user_detail['data']['bank_ifsc'];
        _static_bank_address = user_detail['data']['address'];
    });
    super.initState();
  }

  // ignore: non_constant_identifier_names

  @override
  Widget build(BuildContext context) {


    bank_name = TextEditingController(text: _static_bank_name);

    bank_address = TextEditingController(text: _static_bank_address);

    bank_ifsc_code= TextEditingController(text: _static_bank_ifsc_code);

    bank_ac = TextEditingController(text: _static_bank_ac);

    return Scaffold(
      key: _scaffoldKey,
      appBar:AppHeader("Bank Detail"),
      body:SingleChildScrollView(
          child: Container(
          padding: EdgeInsets.all(7),
          child: Card(
            elevation:3,
            child:Column(
            children:[
              Padding(
                padding: EdgeInsets.all(10),
                child: Form(
                  // ignore: deprecated_member_use
                  autovalidate: onSubmit ? true : false,
                // ignore: deprecated_member_use
                  key:_formKey ,
                  child: Column(
                  children:[
                    TextFormField(
                      controller: bank_name,
                      decoration: InputDecoration(
                        labelText: 'Bank Name'
                      ),
                      onChanged: (value){
                        _static_bank_name = value;
                      },
                      // ignore: missing_return
                      validator: (value){

                        if(value.isEmpty){
                          return "Please enter bank name";
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      controller: bank_ac,
                      keyboardType: TextInputType.number,
                      onChanged: (value){
                        _static_bank_ac = value;
                      },
                      decoration: InputDecoration(
                        labelText: 'A/c number'
                      ),
                      validator: (value){
                        if(value.isEmpty){
                          return "Please enter a/c number";
                        }
                        return null;
                      }
                    ),
                    TextFormField(
                      controller: bank_ifsc_code,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        labelText: 'IFSC Code'
                      ),
                      onChanged: (value){
                        _static_bank_ifsc_code = value;
                      },
                      validator: (value){
                        if(value.isEmpty){
                          return "Please enter IFSC code";
                        }
                        return null;
                      }
                    ),
                    SizedBox(height:17),
                      Material(
                      elevation: 5.0,
                      borderRadius: BorderRadius.circular(5.0),
                      color: Theme.of(context).primaryColor,
                      child: MaterialButton(
                      minWidth: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      onPressed: () async {
                        setState(() {
                          onSubmit = true;
                        });
                        
                        if(_formKey.currentState.validate()){
                            EasyLoading.show(status:"Please wait...");
                            String token = await Provider.of<Auth>(context,listen: false).getToken();

                            try{
                              final Map update_bank_data  = {
                                "bank":bank_name.text,
                                "bank_ac":bank_ac.text,
                                "bank_ifsc":bank_ifsc_code.text,
                                "user_id":user_detail['data']['id'].toString()
                              };
                              http.Response res = await http.post("$api_url/api/cleaner/bank_detail",headers:{
                                "Authorization":"Bearer $token"
                              },body: update_bank_data);
                              Map res_body = jsonDecode(res.body);
                              if(res.statusCode == 200){
                                EasyLoading.dismiss();
                                _showSnackBar(res_body['message'],color: Colors.green);
                              }else{
                                _showSnackBar(res_body['message']);
                              }
                            }catch(e){
                              throw e;
                            }
                            //clean input after submit form
                            Provider.of<Auth>(context,listen: false).getUserApi().then((value){
                              print("Called");
                            }).catchError((e){
                              throw e;
                            });
                            FocusScope.of(context).requestFocus(FocusNode());

                        }
                      },
                          child: Text("Submit",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white, fontWeight: FontWeight.bold)),
                          ),
                        )
                      ],
                    ),
                    )
                  )
                ]
              )
            ),
          ),
      )
      
    );
  }
}