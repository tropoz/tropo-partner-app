import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:troppo_mobile/constants.dart';
import 'package:troppo_mobile/providers/Auth.dart';

import '../../widgets/AppButton.dart';
import 'package:http/http.dart' as http;

class ResetPasswordWithOld extends StatefulWidget {
  ResetPasswordWithOld({Key key}) : super(key: key);

  @override
  _ResetPasswordWithOldState createState() => _ResetPasswordWithOldState();
}

class _ResetPasswordWithOldState extends State<ResetPasswordWithOld> {

  TextEditingController new_password = TextEditingController();

  TextEditingController old_password = TextEditingController();

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();



  TextEditingController confirm_password = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  bool onSubmit = false;

  void _showSnackBar(value,{Color color:Colors.green}) {
    final snackBar = SnackBar(
      content: Text(value),
      backgroundColor: color,
      duration: Duration(seconds: 3),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  bool isVisiblePassword = false;
  @override
  Widget build(BuildContext context) {
    final newPasswordFocus = FocusNode();
    final confirmPasswordFocus = FocusNode();

    final old_password_field = TextFormField(
      controller: old_password,
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (v){
        FocusScope.of(context).requestFocus(newPasswordFocus);
      },
      validator: (value) {
        if (value.trim().isEmpty) {
          return "Old password is required";
        }
        return null;
      },
      obscureText: true,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Old Password",
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0))),
    );
    final new_password_field = TextFormField(
      focusNode: newPasswordFocus,
      controller: new_password,
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (v){
        FocusScope.of(context).requestFocus(confirmPasswordFocus);
      },
      validator: (value) {
        if (value.trim().isEmpty) {
          return "New password is required";
        } else if (value.trim().length < 6) {
          return "Password must be 6 character";
        }
        return null;
      },
      obscureText: isVisiblePassword ? false : true,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          suffixIcon: IconButton(icon: !isVisiblePassword ? Icon(Icons.visibility_off) : Icon(Icons.visibility), onPressed: (){
                setState(() {
                  isVisiblePassword =!isVisiblePassword;
                });
          }),
          hintText: "New Password",
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0))),
    );
    final confirm_password_field = TextFormField(
      focusNode: confirmPasswordFocus,
      controller: confirm_password,
      validator: (value) {
        if (value.trim().isEmpty) {
          return "Confirm password required";
        }else if(value != new_password.text){
          return "Password is n't matching";
        }
        return null;
      },
      obscureText: true,
      onFieldSubmitted: (v){
        FocusScope.of(context).requestFocus(FocusNode());
        
      },
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Confirm Password",
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0))),
    );
  
    return Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(
          child: Padding(
        padding: EdgeInsets.only(top: 60, left: 10, right: 10, bottom: 10),
        child: Form(
          // ignore: deprecated_member_use
          autovalidate: onSubmit,
          key: _formKey,
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                  children: [
                      Icon(Icons.vpn_key,size: 50,),
                      Text("Change Password",style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    )
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height / 4.9,
              ),
              Align(
                child: Text(
                  "Enter Old Password :",
                  textAlign: TextAlign.start,
                ),
                alignment: Alignment.centerLeft,
              ),
              SizedBox(
                height: 4,
              ),
              old_password_field,
              SizedBox(
                height: 20,
              ),
              Align(
                child: Text(
                  "Enter New Password :",
                  textAlign: TextAlign.start,
                ),
                alignment: Alignment.centerLeft,
              ),
              SizedBox(
                height: 4,
              ),
              new_password_field,
              SizedBox(
                height: 20,
              ),
              Align(
                child: Text(
                  "Confirm Password :",
                  textAlign: TextAlign.start,
                ),
                alignment: Alignment.centerLeft,
              ),
              SizedBox(
                height: 4,
              ),
              confirm_password_field,
              SizedBox(height: 20),
              AppButton(text:"Next",method : () async {
                setState(() {
                  onSubmit = true;
                });
                if(_formKey.currentState.validate()){
                  FocusScope.of(context).unfocus();
                  EasyLoading.show(status: "Please wait...");
                  final token  = await Provider.of<Auth>(context,listen: false).getToken();
                  final user = Provider.of<Auth>(context,listen: false).getUser;
                  try{
                    http.Response res = await http.post("$api_url/api/cleaner/password_change",headers: {
                      "Authorization":"Bearer $token"
                    },body:{
                      "current_password":old_password.text,
                      "new_password":new_password.text,
                      "new_password_confirmation":confirm_password.text,
                      "user_id":user['data']['id'].toString()
                    });
                    var resMsg = jsonDecode(res.body);
                    if(res.statusCode == 200){
                      FocusScope.of(context).unfocus();
                      EasyLoading.dismiss();
                      _showSnackBar(resMsg['message']);
                      Future.delayed(Duration(seconds: 2),(){
                        Navigator.pop(context);
                      });


                    }else{
                      EasyLoading.dismiss();
                      _showSnackBar(resMsg['message'],color: Colors.red);
                    }
                  }catch(e){
                    EasyLoading.dismiss();

                    throw e;
                  }
                  // Navigator.pushNamedAndRemoveUntil(context, "/login",(_)=>false);
                }
              }),
            ],
          ),
        ),
      )),
    );
  }
}
