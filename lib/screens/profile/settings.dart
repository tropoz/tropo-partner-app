import 'package:flutter/material.dart';

import '../../widgets/AppHeader.dart';


class Setting extends StatefulWidget {
  Setting({Key key}) : super(key: key);

  @override
  _SettingState createState() => _SettingState();
}

class _SettingState extends State<Setting> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppHeader("Settings"),
      body: Center(
        child: Text("Settings"),
      ),
    );
  }
}