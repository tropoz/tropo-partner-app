import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:troppo_mobile/constants.dart';
import 'package:troppo_mobile/providers/Auth.dart';

import '../../widgets/AppHeader.dart';

import 'dart:io';

import 'package:image_picker/image_picker.dart';


import 'package:http/http.dart' as http;

extension EmailValidator on String {
  bool isValidEmail() {
    return RegExp(
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(this);
  }
}

class ProfileUpdate extends StatefulWidget {
  @override
  _ProfileUpdateState createState() => _ProfileUpdateState();
}

class _ProfileUpdateState extends State<ProfileUpdate> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  File _image;
  var filename;
  final picker = ImagePicker();

  @override
  void didChangeDependencies(){
    print("asdfghjkl");
    super.didChangeDependencies();
  }

  static String _static_name;
  static String _static_email;
  static String _static_phone;


  TextEditingController _email;

  TextEditingController _name;

  TextEditingController _phone;




  Future _submit(user_intance) async {
    setState(() {
      onSubmit = true;
    });
    if (_formKey.currentState.validate()) {
      EasyLoading.show(status:"Loading");
      FocusScope.of(context).unfocus();

  
      var request = http.MultipartRequest(
          'POST', Uri.parse('$api_url/api/cleaner/profile_update'));
      request.fields.addAll({
        'mobile': _phone.text,
        "email": _email.text,
        "name":_name.text
      });
      final token = await user_intance.getToken();
      final headers = {
        "Authorization": "Bearer $token",
        "Accept": "application/json",
        "Content-Type": "multipart/form-data",
      };

      if (_image != null) {

        print(filename);
        var file = await http.MultipartFile.fromPath(
          'avatar',
          filename,
        );
        print(file);
        request.files.add(file);

      }
      request.headers.addAll(headers);
      try{
        http.StreamedResponse response = await request.send();
        print(response.statusCode);
        if (response.statusCode == 200) {
          await Provider.of<Auth>(context,listen: false).getUserApi();
          print(await response.stream.bytesToString());
          EasyLoading.dismiss();
          _showSnackBar("Profile Updated Successfully",color: Colors.green);
        } else {
          var errors = jsonDecode(await response.stream.bytesToString());
          for(final v in errors['errors']['mobile']){
            EasyLoading.dismiss();
            _showSnackBar(v,color: Colors.red);
          }
        }
      }catch(e){
        print(e);
      }
    }
  }

  @override
  void dispose(){
    EasyLoading.dismiss();
    super.dispose();
  }



  @override 
  void initState() {
    Future.delayed(Duration.zero,(){
      final user_detail = Provider.of<Auth>(context,listen: false).getUser;

      
      
      if(user_detail != null){
        setState(() {
          _static_name = user_detail['data']['name'];
          _static_email = user_detail['data']['email'];
          _static_phone = user_detail['data']['mobile'];
        });
      }

    });
    super.initState();
  }

  final _formKey = GlobalKey<FormState>();
  bool onSubmit = false;
  // ignore: non_constant_identifier_names


  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        filename = pickedFile.path;
      } else {
        print('No image selected.');
      }
    });
  }

  void _showSnackBar(value,{Color color:Colors.red}) {
    final snackBar = SnackBar(
      content: Text(value),
      backgroundColor: color,
      duration: Duration(seconds: 3),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }



  @override
  Widget build(BuildContext context) {
    final user_intance = Provider.of<Auth>(context, listen: false);
    _name = TextEditingController(text:_static_name);
    _email = TextEditingController(text:_static_email);
    _phone = TextEditingController(text:_static_phone);

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppHeader("Profile Update"),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(7),
          child: Card(
              elevation: 3,
              child: Column(children: [
                _image == null
                    ? Container(
                        width: 100,
                        height: 100,
                        margin: EdgeInsets.only(top: 30),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.blue,width: 3.0,),
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image: NetworkImage(
                                    '${user_intance.getUser != null ? user_intance.getUser['data']['avatar'] : ""}'))))
                    : Container(
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.blue,width: 3.0,),
                          
                        ),
                        margin: EdgeInsets.only(top: 30),
                        height: 100,
                        width: 100,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(500.0),
                          child: Image.file(
                            _image,
                            fit: BoxFit.cover,
                          ),
                        )),
                Padding(
                    padding: EdgeInsets.all(10),
                    child: Form(
                      // ignore: deprecated_member_use
                      key: _formKey,
                      // ignore: deprecated_member_use
                      autovalidate: onSubmit ? true : false,
                      child: Column(
                        children: [
                          TextFormField(
                            controller: _name,
                            onChanged: (value){
                              _static_name = value;
                              _name.selection = TextSelection.fromPosition(TextPosition(offset: _name.text.length));
                            },
                            decoration: InputDecoration(labelText: 'Name'),
                            // ignore: missing_return
                            validator: (value) {
                              if (value.isEmpty) {
                                return "Please enter name";
                              }
                              return null;
                            },
                          ),
                          TextFormField(
                              controller: _phone,
                              onChanged: (value){
                                _static_phone = value;
                                _phone.selection = TextSelection.fromPosition(TextPosition(offset: _phone.text.length));
                              },
                              keyboardType: TextInputType.phone,
                              decoration: InputDecoration(labelText: 'Mobile No'),
                              validator: (value) {
                                if (value.isEmpty || value.length < 10 || value.length > 10) {
                                  return "Please enter valid mobile no";
                                }
                                return null;
                              }),
                          TextFormField(
                              controller: _email,
                              onChanged: (value){
                                _static_email= value;
                                _email.selection = TextSelection.fromPosition(TextPosition(offset: _email.text.length));
                              },
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(labelText: 'Email'),
                              validator: (value) {
                                if (!value.isValidEmail()) {
                                  return "Please enter valid email";
                                }
                                return null;
                              }),
                          SizedBox(height: 17),
                          Material(
                            elevation: 5.0,
                            borderRadius: BorderRadius.circular(5.0),
                            color: Theme.of(context).primaryColor,
                            child: MaterialButton(
                              minWidth: MediaQuery.of(context).size.width,
                              padding:
                                  EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                              onPressed: () {
                                _submit(user_intance);
                              },
                              child: Text("Submit",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold)),
                            ),
                          )
                        ],
                      ),
                    ))
              ])),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: getImage,
        tooltip: 'Pick Image',
        child: Icon(Icons.add_a_photo),
      ),
    );
  }
}