import 'dart:convert';

import '../../constants.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:troppo_mobile/providers/Auth.dart';
import '../../widgets/AppListView.dart';
import 'package:http/http.dart' as http;

import '../../widgets/Loader.dart';


import '../../providers/Auth.dart';

import '../../widgets/NetWorkIssue.dart';

class Cluster extends StatefulWidget {

  @override
  _ClusterState createState() => _ClusterState();
}



class _ClusterState extends State<Cluster> {

  List clutters = [];

  bool isNetwork = true;
 

  bool isLoading = true;
  void isConnectionMethod() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile || connectivityResult == ConnectivityResult.wifi) {
        setState(() {
          isNetwork = true;
        });
        getClutters();
    } else{
        setState(() {
          isNetwork = false;
        });
    }
  }
  Future getClutters() async{
    var token = "";
    Future.delayed(Duration.zero,() async {
      try{
        token = await Provider.of<Auth>(context,listen: false).getToken();

        http.Response res = await http.get("$api_url/api/cleaner/assigned_cluster",headers:{
          "Accept":"application/json",
          "Authorization" : "Bearer $token"
        });
        var resData = json.decode(res.body);
        print(resData);
        setState(() {
          isLoading = false;
        });
        if(res.statusCode == 200){
          setState(() {
            clutters = resData['data'];
          });
        }

      }
      catch(e){
        throw e;
      }
      
    });

  }

  @override
  void initState() {
    isConnectionMethod();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(70),
        child: AppBar(
        title:Text("My clusters"),
      ),),
      body:isNetwork ? (isLoading ? Loader() : AppListView(clutters.length,clutters)) : NetWorkIssue(isConnectionMethod) 
    );
  }
}