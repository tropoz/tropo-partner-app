import 'package:flutter/material.dart';

import 'dart:async';

import 'package:google_maps_flutter/google_maps_flutter.dart';




class LocationCluster extends StatefulWidget {

  _LocationClutState createState() => _LocationClutState();
}

class _LocationClutState extends State<LocationCluster> {

  Completer<GoogleMapController> _controller = Completer();

  static LatLng _center;
  

  final Set<Marker> _markers = {};
  // ignore: unused_field
  LatLng _lastMapPosition = _center;
  MapType _currentMapType = MapType.normal;

  _onMapCreated(GoogleMapController controller){
    _controller.complete(controller);
  }

  _onCameraMoved(CameraPosition position){
    _lastMapPosition = position.target;
  }

  @override
  Widget build(BuildContext context) {
    final Map pageData =  ModalRoute.of(context).settings.arguments;

    _center = LatLng(double.parse(pageData['cluster']['latitude']),double.parse(pageData['cluster']['longitude']));

    Set<Circle> circles = Set.from([Circle(
        circleId: CircleId("circle"),
        center: LatLng(double.parse(pageData['cluster']['latitude']), double.parse(pageData['cluster']['longitude'])),
        radius:double.parse(pageData['cluster']['radius'].toString()),
        fillColor: Colors.red,
        strokeColor: Color.fromRGBO(171, 39, 133, 0.5),
    )]);
    return Scaffold(
      appBar:PreferredSize(
         preferredSize: Size.fromHeight(70.0),
         child:AppBar(
          title:Text("${pageData['cluster']['name']}"),
          backgroundColor: Theme.of(context).primaryColor,
         )
      ),
      body:Stack(
        children: [
          GoogleMap(
            onMapCreated: _onMapCreated,
            initialCameraPosition:CameraPosition(
            target: _center,
            zoom:14.5,
            ),
            mapType: _currentMapType,
            compassEnabled: true,
            // markers: _markers,
            onCameraMove:_onCameraMoved,
            circles: circles,
          ),
        ],
      ),
    );
  }
}