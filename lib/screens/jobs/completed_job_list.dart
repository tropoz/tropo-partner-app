import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:hexcolor/hexcolor.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';
import 'package:troppo_mobile/constants.dart';
import 'package:troppo_mobile/providers/Auth.dart';

import 'package:troppo_mobile/widgets/Loader.dart';
import 'package:troppo_mobile/widgets/NoDataFound.dart';
import 'package:troppo_mobile/widgets/ShowError.dart';


import '../../widgets/AppHeader.dart';

import '../../widgets/BottomNavigation.dart';

import 'package:troppo_mobile/models/JobModel.dart';



class JobCompletedList extends StatefulWidget {
  JobCompletedList({Key key}) : super(key: key);

  @override
  _JobCompletedListState createState() => _JobCompletedListState();
}

class _JobCompletedListState extends State<JobCompletedList> {
  String _selectedText = "All";
  String token = "";

  bool isLoading;

  List jobs = [];
  String month = "";
  Future getCompletedJobs() async {
      setState(() {
        isLoading = true;
      });
      final String token = await Provider.of<Auth>(context,listen: false).getToken();
        try{
          Response resBody = await get("$api_url/api/cleaner/completed_job",headers: {
            "Accept":"application/json",
            "Authorization":"Bearer $token"
          });
          var response = jsonDecode(resBody.body);
          if(resBody.statusCode == 200){
            setState(() {
              jobs = response['data'];
              print(jobs);
              isLoading = false;
            });
          }
        }
        catch(e){
          print(e);
          setState(() {
            isLoading = false;
          });
          throw e;
        }
  }
  @override
  void initState() {

    getCompletedJobs();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    DateTime now = new DateTime.now();
    return Scaffold(
      appBar:AppHeader("Completed Jobs",false),
      bottomNavigationBar: BottomNavigation(2),
      body:isLoading ? Loader() : Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Padding(
                    padding: EdgeInsets.all(10),
                    child: Row(
                    mainAxisAlignment:MainAxisAlignment.spaceBetween,
                    children: [
                      Text("$_selectedText (Select Month)",style: TextStyle(fontSize: 15,color:HexColor("#5155b1"),fontWeight: FontWeight.bold),),
                      DropdownButton<String>(
                        isDense: true,
                        value:_selectedText,
                        items: <String>["All","Jan", 'Feb', 'Mar', 'Apr',"May","June","July","Aug","Sep","Oct","Nov","Dec"].map((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child:Text(value),

                          );
                        }).toList(),
                        onChanged: (value) {
                          setState(() {
                            _selectedText = value;
                          });

                          if(value == "Jan"){
                            getCompletedJobs().then((value){
                              setState(() {
                                jobs = jobs.where((element) => element['month'].toString() == "01" && element['year'].toString() == now.year.toString()).toList();
                              });
                            });
                         
                          }
                          else if(value == "Feb"){
                            getCompletedJobs().then((value){
                              setState(() {
                                jobs = jobs.where((element) => element['month'].toString() == "02" && element['year'].toString() == now.year.toString()).toList();
                              });
                            });
                          }
                          else if(value == "Mar"){
                            getCompletedJobs().then((value){
                              setState(() {
                                jobs = jobs.where((element) => element['month'].toString() == "03" && element['year'].toString() == now.year.toString()).toList();
                              });
                            });
                          }
                          else if(value == "Apr"){
                            getCompletedJobs().then((value){
                              setState(() {
                                jobs = jobs.where((element) => element['month'].toString() == "04" && element['year'].toString() == now.year.toString()).toList();
                              });
                            });
                          }
                          else if(value == "May"){
                           getCompletedJobs().then((value){
                              setState(() {
                                jobs = jobs.where((element) => element['month'].toString() == "05" && element['year'].toString() == now.year.toString()).toList();
                              });
                            });
                          }
                          else if(value == "June"){
                            getCompletedJobs().then((value){
                              setState(() {
                                jobs = jobs.where((element) => element['month'].toString() == "06" && element['year'].toString() == now.year.toString()).toList();
                              });
                            });
                          }
                          else if(value == "July"){
                            getCompletedJobs().then((value){
                              setState(() {
                                jobs = jobs.where((element) => element['month'].toString() == "07" && element['year'].toString() == now.year.toString()).toList();
                              });
                            });
                          }
                          else if(value == "Aug"){
                            getCompletedJobs().then((value){
                              setState(() {
                                jobs = jobs.where((element) => element['month'].toString() == "08" && element['year'].toString() == now.year.toString()).toList();
                              });
                            });
                          }
                          else if(value == "Sep"){
                            getCompletedJobs().then((value){
                              setState(() {
                                jobs = jobs.where((element) => element['month'].toString() == "09" && element['year'].toString() == now.year.toString()).toList();
                              });
                            });
                          }
                          else if(value == "Oct"){
                            getCompletedJobs().then((value){
                              setState(() {
                                jobs = jobs.where((element) => element['month'].toString() == "10" && element['year'].toString() == now.year.toString()).toList();
                              });
                            });
                          }
                          else if(value == "Nov"){
                            getCompletedJobs().then((value){
                              setState(() {
                                jobs = jobs.where((element) => element['month'].toString() == "11" && element['year'].toString() == now.year.toString()).toList();
                              });
                            });
                          }
                          else if(value == "Dec"){
                            getCompletedJobs().then((value){
                              setState(() {
                                jobs = jobs.where((element) => element['month'].toString() == "12" && element['year'].toString() == now.year.toString()).toList();
                              });
                            });

                            
                          }
                          else{
                            getCompletedJobs();
                          }
                 

                        },
                      )
                    ],
                    ),
                  ),
                   Expanded(child:jobs.length > 0 ? ListView.separated(
                    separatorBuilder: (context,index){
                      return Divider();
                    },
                    itemCount:jobs.length,itemBuilder:(create,index){
                      return Card(
                        elevation: 2,
                        child: Column(
                          children: [
                            ListTile(
                              title: Text("Customer Name :"),
                              subtitle: Text("${jobs[index]['customer_name']}"),
                              trailing: Text("#101",style: TextStyle(color:HexColor('#5155b1'),fontWeight: FontWeight.bold),),
                            ),
                            ListTile(
                              title: Text("Service Name :"),
                              subtitle: Text("${jobs[index]['service_name']}"),
                              trailing: Text("${jobs[index]['date']}",style: TextStyle(color:HexColor('#5155b1'),fontWeight: FontWeight.bold),),
                            ),
                            ListTile(
                              title: Text("Item name"),
                              subtitle: Text("Service Name"),
                            ),
                          ], 
                        ),
                      );
                    }) : NoDataFound(),) ,
                ],
              ),
    );
  }
}