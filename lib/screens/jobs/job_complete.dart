import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';
import 'package:troppo_mobile/constants.dart';
import 'package:troppo_mobile/providers/Auth.dart';
import 'package:troppo_mobile/widgets/AppDialogWidget.dart';
import '../../widgets/AppHeader.dart';

import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import '../../widgets/animations/AnimationRoutes.dart';

import './cash_job.dart';

class JobComplete extends StatefulWidget {
  JobComplete({Key key}) : super(key: key);

  @override
  _JobCompleteState createState() => _JobCompleteState();
}

class _JobCompleteState extends State<JobComplete> {
  double rating;

  Future ratingSubmit() async {
    String token = await Provider.of<Auth>(context, listen: false).getToken();
    Map arguments = ModalRoute.of(context).settings.arguments;

    try {
      // while getting the res
      showAlertDialog(context);
      Response res =
          await post("$api_url/api/cleaner/rate_your_customer", headers: {
        "Authorization": "Bearer $token",
        'Accept': 'application/json'
      }, body: {
        'customer_subscription_id':
            arguments["customer_subscription_id"].toString(),
        'job_card_id': arguments["job_card_id"].toString(),
        'rating': rating.toString(),
      });
      if (res.statusCode == 200) {
        Navigator.pop(context);
        Navigator.pushReplacementNamed(context, "/my_job",
            arguments: {"rating": true});
      }
    } catch (e) {
      Navigator.pop(context);
      throw e;
    }
  }

  @override
  Widget build(BuildContext context) {
    Map arguments = ModalRoute.of(context).settings.arguments;
    return WillPopScope(
      onWillPop: () async {
        Navigator.pushReplacement(context, ScaleRoute(page: JobCash()));
        return;
      },
      child: Scaffold(
        appBar: AppHeader("101 Job Card"),
        body: Container(
          color: HexColor("#5155b1"),
          height: double.infinity,
          child: Column(children: [
            Container(
              color: Colors.white,
              child: Padding(
                padding: EdgeInsets.all(10),
                child: Container(
                  height: MediaQuery.of(context).size.height / 2,
                  padding: EdgeInsets.all(10),
                  color: HexColor("#58e994"),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      FlatButton(
                          padding: EdgeInsets.all(20),
                          onPressed: null,
                          child: FloatingActionButton(
                            backgroundColor: HexColor("#51c955"),
                            onPressed: null,
                            child: Icon(Icons.done),
                          )),
                      Text(
                        "${'Job Completed Successfully'.toUpperCase()}",
                        style: TextStyle(
                            fontSize: 30,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                      Text(
                        "${'Rate your customer & collect payment'.toUpperCase()}",
                        style: TextStyle(
                            color: Colors.red, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Column(mainAxisAlignment: MainAxisAlignment.end, children: [
              Container(
                color: HexColor("#5155b1"),
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                child: Center(
                  child: Column(children: [
                    Text(
                      "RATE YOUR CUSTOMER",
                      style: TextStyle(color: Colors.white),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text("${arguments['customer_name']}",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 20)),
                    SizedBox(
                      height: 20,
                    ),
                    RatingBar.builder(
                      initialRating: 0,
                      minRating: 0,
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      itemCount: 5,
                      itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                      itemBuilder: (context, _) => Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                      onRatingUpdate: (rated) {
                        print(rated);
                        setState(() {
                          rating = rated;
                        });
                      },
                    ),
                    SizedBox(height: 20),
                    Container(
                      padding: EdgeInsets.only(top: 6),
                      child: Material(
                        elevation: 5.0,
                        borderRadius: BorderRadius.circular(5.0),
                        color: HexColor("#58e994"),
                        child: MaterialButton(
                            minWidth: MediaQuery.of(context).size.width,
                            padding:
                                EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                            onPressed: ratingSubmit,
                            child: Text(
                              "Submit",
                              style: TextStyle(color: Colors.white),
                            )),
                      ),
                    )
                  ]),
                ),
              )
            ])
          ]),
        ),
      ),
    );
  }
}
