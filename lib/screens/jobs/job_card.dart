// import 'package:flushbar/flushbar.dart';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';
import 'package:troppo_mobile/providers/Auth.dart';
import 'package:troppo_mobile/providers/JobCardProvider.dart';
import 'package:troppo_mobile/widgets/Loader.dart';
import '../../constants.dart';
import '../../widgets/AppHeader.dart';

import './job_complete.dart';

import 'package:http/http.dart';

import '../../widgets/animations/AnimationRoutes.dart';

import '../../stores/StoreJobStart.dart';

class JobCard extends StatefulWidget {
  JobCard({Key key}) : super(key: key);

  @override
  _JobCardState createState() => _JobCardState();
}

class _JobCardState extends State<JobCard> {
  bool startJob = false;

  Map job_card = {};

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  bool isLoading = true;

  JobCardProvider jobCardProvider;

  void _onLoading() async {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding: EdgeInsets.all(15),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircularProgressIndicator(),
                  Text("   Please Wait..."),
                ],
              )),
        );
      },
    );

    final String token =
        await Provider.of<Auth>(context, listen: false).getToken();

    List wash_id = [];
    job_card['wash'].forEach((value) {
      wash_id.add(value['wash_id'].toString());
    });
    try {
      Map body = {
        "customer_subscription_id": job_card["customer_subscription_id"],
        "job_card_id": job_card['job_card_id'].toString(),
        "wash_id": wash_id
      };
      Response res = await post("$api_url/api/cleaner/end_job",
          headers: {
            "Authorization": "Bearer $token",
            'Content-type': 'application/json',
            'Accept': 'application/json'
          },
          body: jsonEncode(body));

      if (res.statusCode == 200) {
        Navigator.pop(context);
        Navigator.pushNamed(context, "/job/complete", arguments: {
          "customer_subscription_id": job_card['customer_subscription_id'],
          "job_card_id": job_card["job_card_id"],
          "customer_name": job_card['customer_name']
        });
      }
    } catch (e) {
      throw e;
    }
  }

  void getJobsData() {
    Future.delayed(Duration.zero, () async {
      var job_data =
          (ModalRoute.of(context).settings.arguments as Map)['jobs_data'];
      print(job_data);
      final String token =
          await Provider.of<Auth>(context, listen: false).getToken();
      try {
        Response res = await post("$api_url/api/cleaner/job_card", headers: {
          "Authorization": "Bearer $token"
        }, body: {
          "customer_subscription_id":
              job_data['customer_subscription_id'].toString(),
          "job_card_id": job_data['job_card_id'].toString(),
        });
        var data = jsonDecode(res.body);
        print("Job card data : $data");

        if (res.statusCode == 200) {
          setState(() {
            job_card = data['data'];
            isLoading = false;
          });
        }
      } catch (e) {
        throw e;
      }
    });
  }

  void _showSnackBar(value, {Color color: Colors.red}) {
    final snackBar = SnackBar(
      content: Text(value),
      backgroundColor: color,
      duration: Duration(seconds: 3),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  @override
  void initState() {
    StoreJobStart.getStartJob().then((value) {
      if (value != null) {
        setState(() {
          startJob = true;
        });
      }
    }).catchError((e) {
      throw e;
    });

    getJobsData();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<Auth>(context).getUser;

    final jobCardProvider = Provider.of<JobCardProvider>(context);

    var image =
        "https://i.picsum.photos/id/9/250/250.jpg?hmac=tqDH5wEWHDN76mBIWEPzg1in6egMl49qZeguSaH9_VI";
    if (user != null) {
      image = user['data']['avatar'];
    }
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppHeader("101 Job Card"),
      body: isLoading
          ? Loader()
          : Stack(
              children: [
                SingleChildScrollView(
                  child: Container(
                    padding: EdgeInsets.only(top: 70),
                    color: HexColor("#F7F7F7"),
                    child: Container(
                      color: Colors.white,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                              padding: EdgeInsets.all(10),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "${job_card['service_name']}",
                                        style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Text("${job_card['plan_name']}"),
                                      SizedBox(height: 20),
                                      Text("${job_card['vehicle_name']}"),
                                    ],
                                  ),
                                  SizedBox(
                                      height: 100,
                                      width: 100,
                                      child: Image.network(
                                        "$image",
                                        errorBuilder: (BuildContext context,
                                            Object exception,
                                            StackTrace stackTrace) {
                                          return Center(
                                              child:
                                                  Text("Couldn't load image"));
                                        },
                                      ))
                                ],
                              )),

                          Divider(),

                          Column(
                            children: [
                              ListTile(
                                leading: Icon(Icons.account_circle),
                                title: Text("${job_card['customer_name']}"),
                              ),
                              ListTile(
                                leading: Icon(Icons.phone),
                                title: Text(
                                  "${job_card['customer_mobile']}",
                                  style: TextStyle(color: Colors.red),
                                ),
                              ),
                              Divider(),
                              ListTile(
                                leading: Icon(Icons.location_city),
                                title: Text("${job_card['customer_address']}"),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              color: Colors.white,
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text("Pakage Included"),
                                    ListView.builder(
                                      shrinkWrap: true,
                                      itemCount: job_card['inclusion'].length,
                                      itemBuilder: (context, index) {
                                        print(index);
                                        return ListTile(
                                          leading: Icon(
                                              Icons.radio_button_unchecked),
                                          title: Text(
                                              "${job_card['inclusion'][index]}"),
                                        );
                                      },
                                    )
                                  ]),
                            ),
                          ),

                          job_card['status'].toString() == "0"
                              ? Padding(
                                  padding: EdgeInsets.all(10),
                                  child: Card(
                                    child: Padding(
                                      padding: EdgeInsets.all(10),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          job_card['on_demand'].toString() ==
                                                  "1"
                                              ? RaisedButton(
                                                  onPressed: () {
                                                    Navigator.pushNamed(context,
                                                        "/jobs/post-photo");
                                                  },
                                                  child: Text("OTP"))
                                              : Text(""),
                                          RaisedButton(
                                            onPressed: () async {
                                              final String token =
                                                  await Provider.of<Auth>(
                                                          context,
                                                          listen: false)
                                                      .getToken();

                                              List wash_id = [];
                                              job_card['wash'].forEach((value) {
                                                wash_id.add(value['wash_id']
                                                    .toString());
                                              });
                                              try {
                                                Map body = {
                                                  "customer_subscription_id":
                                                      job_card[
                                                          "customer_subscription_id"],
                                                  "job_card_id":
                                                      job_card['job_card_id']
                                                          .toString(),
                                                  "wash_id": wash_id
                                                };
                                                Response res = await post(
                                                    "$api_url/api/cleaner/start_job",
                                                    headers: {
                                                      "Authorization":
                                                          "Bearer $token",
                                                      'Content-type':
                                                          'application/json',
                                                      'Accept':
                                                          'application/json'
                                                    },
                                                    body: jsonEncode(body));

                                                if (res.statusCode == 200) {
                                                  getJobsData();
                                                }
                                              } catch (e) {
                                                throw e;
                                              }
                                            },
                                            textColor: Colors.white,
                                            padding: const EdgeInsets.all(0.0),
                                            child: Container(
                                              decoration: const BoxDecoration(
                                                gradient: LinearGradient(
                                                  colors: <Color>[
                                                    Colors.green,
                                                    Colors.green,
                                                    Colors.green,
                                                  ],
                                                ),
                                              ),
                                              padding:
                                                  const EdgeInsets.all(10.0),
                                              child: const Text('START JOB'),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ))
                              : Padding(
                                  padding: EdgeInsets.all(8),
                                  child: Card(
                                    child: Padding(
                                      padding: EdgeInsets.all(10),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          job_card['on_demand'].toString() ==
                                                  "1"
                                              ? RaisedButton(
                                                  onPressed: () {
                                                    //  Navigator.pushNamed(context, "/jobs/post-photo");
                                                  },
                                                  child: Text("OTP"))
                                              : Text(""),
                                          RaisedButton(
                                            onPressed: () {
                                              _onLoading();

                                              // Future.delayed(Duration(seconds:3),() async {
                                              //   try{
                                              //     await StoreJobStart.removeJob();
                                              //     setState(() {
                                              //       startJob = false;
                                              //     });
                                              //     Navigator.pop(context);
                                              //     Navigator.pushReplacement(context,SlideRightRoute(page:JobComplete()));
                                              //   }catch(e){
                                              //     throw e;
                                              //   }
                                              // });
                                            },
                                            textColor: Colors.white,
                                            color: Colors.red,
                                            child: Text("End Job"),
                                          ),
                                        ],
                                      ),
                                    ),
                                  )),
                          Padding(
                              padding: EdgeInsets.all(10),
                              child: Card(
                                  child: ListTile(
                                title: Text("Other Instruction",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.red)),
                                subtitle:
                                    Text("${job_card['other_instruction']}"),
                              ))),
                          //wash type 1
                          if (job_card['status'].toString() == "1")
                            Padding(
                                padding: EdgeInsets.all(10),
                                child: Card(
                                    child: Column(children: [
                                  ...(job_card['wash'] as List).map((e) {
                                    var index = job_card['wash'].indexOf(e);

                                    return Column(
                                      children: [
                                        Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Column(children: [
                                                Text("${e['name']}",
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color: Colors.red)),
                                                ...(e['bullet']).map((element) {
                                                  return Text(
                                                      element.toString());
                                                }),
                                              ]),
                                              jobCardProvider.isCompletedJob ||
                                                      e['status'].toString() ==
                                                          "4"
                                                  ? Padding(
                                                      padding:
                                                          EdgeInsets.all(10),
                                                      child: RaisedButton(
                                                        onPressed: () async {},
                                                        textColor: Colors.white,
                                                        padding:
                                                            const EdgeInsets
                                                                .all(0.0),
                                                        child: Container(
                                                          decoration:
                                                              BoxDecoration(
                                                            gradient:
                                                                LinearGradient(
                                                              colors: [
                                                                Colors.green,
                                                                Colors.green,
                                                                Colors.green,
                                                              ],
                                                            ),
                                                          ),
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(10.0),
                                                          child: Text(
                                                              "Completed job"),
                                                        ),
                                                      ))
                                                  : Padding(
                                                      padding:
                                                          EdgeInsets.all(10),
                                                      child: RaisedButton(
                                                        onPressed: () async {
                                                          if (e['status']
                                                                  .toString() !=
                                                              "3") {
                                                            var qr_code_decode =
                                                                await Navigator
                                                                    .pushNamed(
                                                                        context,
                                                                        "/qr_code/scanner");
                                                            Map body = {
                                                              "customer_subscription_id":
                                                                  job_card[
                                                                          'customer_subscription_id']
                                                                      .toString(),
                                                              "job_card_id": job_card[
                                                                      'job_card_id']
                                                                  .toString(),
                                                              "job_card_activity_id":
                                                                  e["job_card_activity_id"]
                                                                      .toString(),
                                                              "wash_type":
                                                                  e['wash_type']
                                                                      .toString(),
                                                              'wash_id': e[
                                                                      'wash_id']
                                                                  .toString(),
                                                              "qrcode": (qr_code_decode
                                                                          as Map)[
                                                                      'code']
                                                                  .toString()
                                                            };
                                                            if (qr_code_decode !=
                                                                null) {
                                                              String token = await Provider.of<
                                                                          Auth>(
                                                                      context,
                                                                      listen:
                                                                          false)
                                                                  .getToken();
                                                              try {
                                                                Response res =
                                                                    await post(
                                                                        "$api_url/api/cleaner/start_wash",
                                                                        headers: {
                                                                          "Authorization":
                                                                              "Bearer $token"
                                                                        },
                                                                        body:
                                                                            body);
                                                                var res_body =
                                                                    jsonDecode(
                                                                        res.body);
                                                                print(res_body);
                                                                if (res.statusCode ==
                                                                    200) {
                                                                  showDialog(
                                                                      context:
                                                                          context,
                                                                      child:
                                                                          new AlertDialog(
                                                                        title: Text(
                                                                            "Please upload images before wash!"),
                                                                        actions: [
                                                                          FlatButton(
                                                                            child:
                                                                                Text("OK"),
                                                                            onPressed:
                                                                                () {
                                                                              Navigator.pop(context);
                                                                              Navigator.pushNamed(context, "/jobs/pre-photo", arguments: {
                                                                                "customer_subscription_id": job_card['customer_subscription_id'].toString(),
                                                                                "job_card_id": job_card['job_card_id'].toString(),
                                                                                "job_card_activity_id": e["job_card_activity_id"].toString(),
                                                                                "wash_type": e['wash_type'].toString(),
                                                                                'wash_id': e['wash_id'].toString(),
                                                                                "status": job_card['status'].toString(),
                                                                                "wash_status": e['status'].toString(),
                                                                              });
                                                                            },
                                                                          ),
                                                                        ],
                                                                      ));
                                                                } else {
                                                                  _showSnackBar(
                                                                      res_body[
                                                                          'message']);
                                                                }
                                                              } catch (e) {
                                                                throw e;
                                                              }
                                                            }
                                                          } else {
                                                            print(
                                                                "End wash button raised");
                                                            var a = await Navigator
                                                                .pushNamed(
                                                                    context,
                                                                    "/jobs/post-photo",
                                                                    arguments: {
                                                                  "customer_subscription_id":
                                                                      job_card[
                                                                              'customer_subscription_id']
                                                                          .toString(),
                                                                  "job_card_id":
                                                                      job_card[
                                                                              'job_card_id']
                                                                          .toString(),
                                                                  "job_card_activity_id":
                                                                      e["job_card_activity_id"]
                                                                          .toString(),
                                                                  "wash_type": e[
                                                                          'wash_type']
                                                                      .toString(),
                                                                  'wash_id': e[
                                                                          'wash_id']
                                                                      .toString(),
                                                                  "status": job_card[
                                                                          'status']
                                                                      .toString(),
                                                                  "wash_status":
                                                                      e['status']
                                                                          .toString(),
                                                                }) as Map;
                                                            if (a != null &&
                                                                a['status_uploaded_image']) {
                                                              print(
                                                                  "Stauts post images ${a['status_uploaded_image']}");
                                                              getJobsData();
                                                            }
                                                          }
                                                        },
                                                        textColor: Colors.white,
                                                        padding:
                                                            const EdgeInsets
                                                                .all(0.0),
                                                        child: Container(
                                                          decoration:
                                                              BoxDecoration(
                                                            gradient:
                                                                LinearGradient(
                                                              colors: e['status']
                                                                          .toString() ==
                                                                      "3"
                                                                  ? [
                                                                      Colors
                                                                          .red,
                                                                      Colors
                                                                          .red,
                                                                      Colors
                                                                          .red,
                                                                    ]
                                                                  : [
                                                                      Colors
                                                                          .lime,
                                                                      Colors
                                                                          .lime,
                                                                      Colors
                                                                          .lime,
                                                                    ],
                                                            ),
                                                          ),
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(10.0),
                                                          child: jobCardProvider
                                                                      .isEndWash ||
                                                                  e['status']
                                                                          .toString() ==
                                                                      '3'
                                                              ? Text("END WASH")
                                                              : Text(
                                                                  "START WASH"),
                                                        ),
                                                      )),
                                            ]),
                                        Padding(
                                            padding: EdgeInsets.all(10),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                Text("QR CODE"),
                                                FloatingActionButton(
                                                  onPressed: () async {
                                                    String token =
                                                        await Provider.of<Auth>(
                                                                context,
                                                                listen: false)
                                                            .getToken();
                                                    var a = await Navigator
                                                        .pushNamed(context,
                                                            "/jobs/pre-photo",
                                                            arguments: {
                                                          "customer_subscription_id":
                                                              job_card[
                                                                      'customer_subscription_id']
                                                                  .toString(),
                                                          "job_card_id": job_card[
                                                                  'job_card_id']
                                                              .toString(),
                                                          "job_card_activity_id":
                                                              e["job_card_activity_id"]
                                                                  .toString(),
                                                          "wash_type":
                                                              e['wash_type']
                                                                  .toString(),
                                                          'wash_id':
                                                              e['wash_id']
                                                                  .toString(),
                                                          "status": e['status']
                                                              .toString(),
                                                          "wash_status":
                                                              e['status']
                                                                  .toString(),
                                                          "display": true
                                                        }) as Map;
                                                    print("Data came from $a");
                                                    if (a != null &&
                                                        a["status_uploaded_image"]) {
                                                      getJobsData();
                                                    }
                                                  },
                                                  child: Icon(Icons.add),
                                                  mini: true,
                                                  backgroundColor: Colors.red,
                                                  heroTag: "btn-pre${index}",
                                                  tooltip: "Pre Photos",
                                                ),
                                                FloatingActionButton(
                                                    onPressed: () {
                                                      Navigator.pushNamed(
                                                          context,
                                                          "/jobs/post-photo",
                                                          arguments: {
                                                            "customer_subscription_id":
                                                                job_card[
                                                                        'customer_subscription_id']
                                                                    .toString(),
                                                            "job_card_id": job_card[
                                                                    'job_card_id']
                                                                .toString(),
                                                            "job_card_activity_id":
                                                                e["job_card_activity_id"]
                                                                    .toString(),
                                                            "wash_type":
                                                                e['wash_type']
                                                                    .toString(),
                                                            'wash_id':
                                                                e['wash_id']
                                                                    .toString(),
                                                            "status": job_card[
                                                                    'status']
                                                                .toString(),
                                                            "wash_status":
                                                                e['status']
                                                                    .toString(),
                                                            "display": true
                                                          });
                                                    },
                                                    child: Icon(Icons.add),
                                                    mini: true,
                                                    backgroundColor: Colors.red,
                                                    heroTag: "btn-post${index}",
                                                    tooltip: "Post Photos"),
                                              ],
                                            ))
                                      ],
                                    );
                                  }),
                                ]))),
                          // wash type second
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                    color: job_card['status'].toString() == "1"
                        ? HexColor("#58E380")
                        : HexColor("#808080"),
                    padding: EdgeInsets.all(8),
                    height: MediaQuery.of(context).size.height / 11,
                    child: Center(
                        child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "${job_card['status'].toString() == "1" ? "You are on job" : "Your job is off,start first"}",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ))),
              ],
            ),
    );
  }
}
