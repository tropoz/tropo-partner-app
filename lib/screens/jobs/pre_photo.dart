
import 'dart:convert';

import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:google_map_polyline/google_map_polyline.dart';
// import 'package:http/http.dart';
import 'package:dio/dio.dart';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:troppo_mobile/constants.dart';
import 'package:troppo_mobile/providers/Auth.dart';
import 'package:troppo_mobile/providers/JobCardProvider.dart';
import 'package:troppo_mobile/widgets/AppDialogWidget.dart';
import 'package:troppo_mobile/widgets/Loader.dart';
import 'package:troppo_mobile/widgets/NoDataFound.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart' as syspaths;
import '../../stores/StorePhotos.dart';
import 'dart:io';



class PrePhoto extends StatefulWidget {
  String a = "";
  PrePhoto({this.a = "fsdfa"});

  @override
  _PrePhotoState createState() => _PrePhotoState();

}

class _PrePhotoState extends State<PrePhoto> {
  final picker = ImagePicker();

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();


  List photos = [];

  bool isLoading = true;


  bool get_photo = false;

  bool callApi = true;

  bool upload_image = false;

  List images = [];


  

  Widget done_photos(BuildContext context){
    if(get_photo && photos.length > 0){
      return IconButton(icon: Icon(Icons.publish,color: Colors.white), onPressed: (){        
        Future.delayed(Duration(seconds:2),(){
          Navigator.pop(context);
          setState(() {
            get_photo = false;
          });
        });
      },);
    }else{
      return Text("");
    }

  }

  void _showSnackBar(value,{Color color:Colors.red}) {
    final snackBar = SnackBar(
      content: Text(value),
      backgroundColor: color,
      duration: Duration(seconds: 3),
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  Future upload_pre_photos(BuildContext context) async {
    String token = await Provider.of<Auth>(context,listen:false).getToken();
    Map route_arguments = ModalRoute.of(context).settings.arguments;
    JobCardProvider jobCardProvider = Provider.of<JobCardProvider>(context);
    // List photos_list = [];
    // photos.forEach((element) {
    //   photos_list.add(File(element['store_file'].toString()).path);
    // });
    // print(photos_list);
    if(photos.length > 0){
      try{
        Response response;
        Dio dio = new Dio();
        showAlertDialog(context);
        List photos_list = [];

        for(int i =0;i < photos.length;i++){
          photos_list.add(await MultipartFile.fromFile(photos[i]['store_file'],filename:photos[i]['filename']));
        }

        print(photos_list);
        FormData formData = new FormData.fromMap({
          'customer_subscription_id': route_arguments['customer_subscription_id'].toString(),
          "job_card_id":route_arguments['job_card_id'].toString(),
          "job_card_activity_id":route_arguments["job_card_activity_id"].toString(),
          "wash_id":route_arguments["wash_id"].toString(),
          "wash_type":route_arguments["wash_type"].toString(),
          "type":"0",
          "image":photos_list
        });
        print(formData);
        response = await dio.post("$api_url/api/cleaner/upload_image",data: formData,
        options:Options(
          headers: {
          "Authorization": "Bearer $token",
          "Accept":"application/json",
          }
        ));

        if(response.statusCode == 200){
            print(response);
           for(var element in photos){
             File(element['store_file']).delete().then((value){
                      print("$value");
              }).catchError((e)=>null);
           }
          await StorePhotos.deletePhotosOfPrePrefernces(route_arguments['wash_id']);
          Navigator.pop(context);

          _showSnackBar(response.data['message'],color: Colors.green);
          jobCardProvider.setEndWash();
          Navigator.pop(context,{
            "status_uploaded_image":true
          });
          getIMages();
        }

      }catch(e){
        if(e is DioError){
          print(e.response);
        }
        Navigator.pop(context);
        print(e);
      }
      // var request = MultipartRequest(
      //     'POST', Uri.parse('$api_url/api/cleaner/upload_image'));
      // request.fields.addAll({
      //   'customer_subscription_id': route_arguments['customer_subscription_id'].toString(),
      //   "service_day_id":route_arguments['service_day_id'].toString(),
      //   "activity_id":route_arguments["activity_id"].toString(),
      //   "wash_id":route_arguments["wash_id"].toString(),
      //   "wash_type":route_arguments["wash_type"].toString(),
      //   "type":"0",
      // });
      // var multipartFile;
      // for(int i=0;i<photos.length;i++)
      // {
      
      // // get file length
      //   print("Run in loop $i");
      //   var multipartFile = await MultipartFile.fromPath(
      //       'image[$i]',
      //       photos[i]['store_file'],
      //     );
      // }


      // final headers = {
      //   "Authorization": "Bearer $token",
      //   "Accept": "application/json",
      //   "Content-Type": "multipart/form-data",
      // };

      // request.headers.addAll(headers);
      // try{
      //   StreamedResponse res = await request.send();
      //   print(await res.stream.bytesToString());
      // }catch(e){
      //   throw e;
      // }
      // var file = await MultipartFile.fromPath(field, filePath);
    }
  }

  Future getImagesUploaded(context) async {
    final Map job_card = ModalRoute.of(context).settings.arguments;


    print(job_card);
    final String token = await Provider.of<Auth>(context,listen: false).getToken();
    if(job_card['wash_status'].toString() == "3" || job_card['wash_status'].toString() == "4"){
      try{
        final Map data = {
          'customer_subscription_id' : job_card["customer_subscription_id"].toString(),
          'job_card_id' : job_card["job_card_id"].toString(),
          'job_card_activity_id' : job_card["job_card_activity_id"].toString(),
          'wash_id' : job_card["wash_id"].toString(),
          'wash_type' : job_card["wash_type"].toString(),
          'type' : "0"
        };
        http.Response res = await http.post("$api_url/api/cleaner/wash_image",headers: {
          "Authorization":"Bearer $token",
        },body: data);
        print("Wash images ${jsonDecode(res.body)}");
        print(res.statusCode);
        if(res.statusCode == 200){
          setState(() {
            images = jsonDecode(res.body)['data'];
          });
        }
      }catch(e){
        setState(() {
          isLoading = false;
        });
        throw e;
      }
    }

    setState(() {
      isLoading = false;
    });

  }
  Future getIMages() async {

    final Map job_card = ModalRoute.of(context).settings.arguments;
    final List lists_photos = await StorePhotos.getPhotosOfPrePrefernces(job_card['wash_id']);
    print(lists_photos);
    setState(() {
      photos = lists_photos;
    });
  }
  @override
  void initState(){
    super.initState();

    Future.delayed(Duration.zero,(){
      
      getIMages();

      getImagesUploaded(context);
    });
  }
  @override
  Widget build(BuildContext context) {
    final Map job_card = ModalRoute.of(context).settings.arguments;
    print(job_card);
    return  WillPopScope(
      onWillPop: (){
        // job_card['wash_status'].toString() != "3" && job_card['wash_status'].toString() != "4") || (job_card['status'].toString() != "3" || 
        if((job_card['display'] == null)){
          showDialog(
            context: context, 
            child:new AlertDialog(
                title: Text("Please upload image first!"),
                content: Text("You can't go before pick any image."),
                actions: [
                  FlatButton(
                    child: Text("OK"),
                    onPressed: () { 
                      Navigator.pop(context);
                    },
                  ),
                ],
              )
          );
        // Navigator.pop(context);

        }else{
          Navigator.pop(context);
        }
      },
      child: Scaffold(
        key: _scaffoldKey,
        appBar:PreferredSize(
           preferredSize: Size.fromHeight(70.0),
           child:AppBar(
            title:Text("Pre Photos"),
            actions: [
              photos.length > 0 ? IconButton(
                icon: Icon(Icons.upload_file),
                onPressed:(){
                  upload_pre_photos(context);
                },
              ) : Text(""),
            ],
            // backgroundColor: Theme.of(context).primaryColor,
         )
      ),
        floatingActionButton: job_card['wash_status'] == "3"  || photos.length == 4 ? null : FloatingActionButton(
          heroTag: "btn",
          onPressed: () async {
            final pickedFile = await picker.getImage(source: ImageSource.camera);

            if(pickedFile != null){
              final local_path = File(pickedFile.path);
              setState(() {
                get_photo = true;
              });
              final appDir = await syspaths.getApplicationDocumentsDirectory();

              print(appDir.path);

              final file_path = path.basename(pickedFile.path);

              final savedFile = await local_path.copy("${appDir.path}/$file_path");
              final List lists_photos = await StorePhotos.getPhotosOfPrePrefernces(job_card['wash_id']);
              Map obj = {
                "store_file":savedFile.path.toString(),
                "filename":pickedFile.path.toString()
              };
              lists_photos.add(obj);
              StorePhotos.saveImageOfPreToPreferences(lists_photos,job_card['wash_id']).then((value){
                  if(value){
                    print("Added successfully");
                    getIMages();
                  }
              }).catchError((error){
                print("error $error");
              });
              print(savedFile);

            }
          },
          child:Icon(Icons.add_a_photo)
        ),
        body: isLoading ? Loader() : images.length > 0 ? ListView.builder(
          itemCount: images.length,
          itemBuilder:(_,index){
            return  Card(
            child:Stack(
              children:[
                SizedBox(
                  child:Image.network(images[index],height: 220,fit: BoxFit.cover,width: MediaQuery.of(context).size.width,)
                ),
              ]
            )
          );
        }) : photos.length > 0 ? ListView.builder(
          itemCount: photos.length,
          itemBuilder:(_,index){
          return Card(
            child:Stack(
              children:[
                SizedBox(
                  child:Image.file(File(photos[index]['store_file']),height: 220,fit: BoxFit.cover,width: MediaQuery.of(context).size.width,)
                ),
               Padding(
                 padding: EdgeInsets.all(5),
                 child: Align(alignment: Alignment.topRight,child:FloatingActionButton(onPressed: () async {
                   setState(() {
      
                    File(photos[index]['store_file']).delete().then((value){
                      print("$value");
                    }).catchError((e)=>null);
                    photos.removeAt(index);
                    StorePhotos.saveImageOfPreToPreferences(photos,job_card['wash_id']).then((value){
                        if(value){
                          
                          print("Deleted successfully");
                        }
                    }).catchError((error){
                      print("error $error");
                    });

                   });
                   
                 },child:Icon(Icons.remove),backgroundColor: Colors.red,mini: true,heroTag: "btn$index",)),
               )
              ]
            )
          );
        }) : NoDataFound()
      ),
    );
  }
}