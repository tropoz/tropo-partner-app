import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:troppo_mobile/providers/Auth.dart';
import 'package:troppo_mobile/providers/JobCardProvider.dart';
import 'package:troppo_mobile/widgets/AppDialogWidget.dart';
import 'package:troppo_mobile/widgets/Loader.dart';
import 'package:troppo_mobile/widgets/NoDataFound.dart';
import 'package:http/http.dart' as http;

import 'package:path/path.dart' as path;

import 'package:path_provider/path_provider.dart' as syspaths;

import '../../constants.dart';
import '../../stores/StorePhotos.dart';

import 'dart:io';

class PostPhoto extends StatefulWidget {
  PostPhoto({Key key}) : super(key: key);

  @override
  _PostPhotoState createState() => _PostPhotoState();
}

class _PostPhotoState extends State<PostPhoto> {
  final picker = ImagePicker();

  List photos = [];

  bool get_photo = false;

  bool upload_image = false;

  List images = [];

  bool isLoading = true;

  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Padding(
              padding: EdgeInsets.all(15),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircularProgressIndicator(),
                  Text("   Please Wait..."),
                ],
              )),
        );
      },
    );
  }

  Widget done_photos() {
    if (get_photo && photos.length > 0) {
      return IconButton(
        icon: Icon(Icons.publish, color: Colors.white),
        onPressed: () {
          _onLoading();

          Future.delayed(Duration(seconds: 2), () {
            Navigator.pop(context);
            setState(() {
              get_photo = false;
            });
          });
        },
      );
    } else {
      return Text("");
    }
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  void _showSnackBar(value) {
    final snackBar = SnackBar(
      backgroundColor: Colors.red,
      content: Text(value),
      duration: Duration(seconds: 3),
      // behavior: SnackBarBehavior.floating,
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  Future upload_pre_photos(BuildContext context) async {
    String token = await Provider.of<Auth>(context, listen: false).getToken();
    Map route_arguments = ModalRoute.of(context).settings.arguments;

    JobCardProvider jobCardProvider = Provider.of<JobCardProvider>(context);

    // List photos_list = [];
    // photos.forEach((element) {
    //   photos_list.add(File(element['store_file'].toString()).path);
    // });
    // print(photos_list);
    if (photos.length > 0) {
      try {
        Response response;
        Dio dio = new Dio();
        showAlertDialog(context);
        List photos_list = [];

        for (int i = 0; i < photos.length; i++) {
          photos_list.add(await MultipartFile.fromFile(photos[i]['store_file'],
              filename: photos[i]['filename']));
        }

        print(photos_list);
        FormData formData = new FormData.fromMap({
          'customer_subscription_id':
              route_arguments['customer_subscription_id'].toString(),
          "job_card_id": route_arguments['job_card_id'].toString(),
          "job_card_activity_id":
              route_arguments["job_card_activity_id"].toString(),
          "wash_id": route_arguments["wash_id"].toString(),
          "wash_type": route_arguments["wash_type"].toString(),
          "type": "1",
          "image": photos_list
        });
        print(formData);
        response = await dio.post("$api_url/api/cleaner/upload_image",
            data: formData,
            options: Options(headers: {
              "Authorization": "Bearer $token",
              "Accept": "application/json",
            }));

        if (response.statusCode == 200) {
          for (var element in photos) {
            File(element['store_file']).delete().then((value) {
              print("Deleted Post photo $value");
            }).catchError((e) => null);
          }
          await StorePhotos.deletePhotosOfPostPrefernces(
              route_arguments['wash_id']);
          Navigator.pop(context);

          // _showSnackBar(response.data['message'],color: Colors.green);

          jobCardProvider.setCompletedJob();
          Navigator.pop(context, {"status_uploaded_image": true});
          getIMages();
        }
      } catch (e) {
        if (e is DioError) {
          print(e.response);
        }

        Navigator.pop(context);

        _showSnackBar("Something went wrong!Please try again");

        print(e);
      }
      // var request = MultipartRequest(
      //     'POST', Uri.parse('$api_url/api/cleaner/upload_image'));
      // request.fields.addAll({
      //   'customer_subscription_id': route_arguments['customer_subscription_id'].toString(),
      //   "service_day_id":route_arguments['service_day_id'].toString(),
      //   "activity_id":route_arguments["activity_id"].toString(),
      //   "wash_id":route_arguments["wash_id"].toString(),
      //   "wash_type":route_arguments["wash_type"].toString(),
      //   "type":"0",
      // });
      // var multipartFile;
      // for(int i=0;i<photos.length;i++)
      // {

      // // get file length
      //   print("Run in loop $i");
      //   var multipartFile = await MultipartFile.fromPath(
      //       'image[$i]',
      //       photos[i]['store_file'],
      //     );
      // }

      // final headers = {
      //   "Authorization": "Bearer $token",
      //   "Accept": "application/json",
      //   "Content-Type": "multipart/form-data",
      // };

      // request.headers.addAll(headers);
      // try{
      //   StreamedResponse res = await request.send();
      //   print(await res.stream.bytesToString());
      // }catch(e){
      //   throw e;
      // }
      // var file = await MultipartFile.fromPath(field, filePath);
    }
  }

  Future getIMages() async {
    Map route_argu = ModalRoute.of(context).settings.arguments;
    final List lists_photo =
        await StorePhotos.getPhotosOfPostPrefernces(route_argu['wash_id']);
    if (route_argu['wash_status'] == "4") {
      final String token =
          await Provider.of<Auth>(context, listen: false).getToken();
      try {
        final Map data = {
          'customer_subscription_id':
              route_argu["customer_subscription_id"].toString(),
          'job_card_id': route_argu["job_card_id"].toString(),
          'job_card_activity_id': route_argu["job_card_activity_id"].toString(),
          'wash_id': route_argu["wash_id"].toString(),
          'wash_type': route_argu["wash_type"].toString(),
          'type': "1"
        };
        http.Response res = await http.post("$api_url/api/cleaner/wash_image",
            headers: {
              "Authorization": "Bearer $token",
            },
            body: data);
        print(jsonDecode(res.body));
        if (res.statusCode == 200) {
          setState(() {
            images = jsonDecode(res.body)['data'];
          });
        }
      } catch (e) {
        setState(() {
          isLoading = false;
        });
        throw e;
      }

      setState(() {
        isLoading = false;
      });
      return;
    }
    print("Before get ");
    final List lists_photos =
        await StorePhotos.getPhotosOfPostPrefernces(route_argu['wash_id']);
    setState(() {
      photos = lists_photos;
    });
  }

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      getIMages();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final Map job_card = ModalRoute.of(context).settings.arguments;

    print(job_card['wash_status']);

    return WillPopScope(
        onWillPop: () {
          if (job_card['wash_status'].toString() == "4" ||
              job_card['display'] != null) {
            Navigator.pop(context);
          } else {
            showDialog(
                context: context,
                child: new AlertDialog(
                  title: Text("Please upload image first!"),
                  content: Text("You can't go before pick any image."),
                  actions: [
                    FlatButton(
                      child: Text("OK"),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ],
                ));
          }
        },
        child: Scaffold(
            key: _scaffoldKey,
            appBar: PreferredSize(
                preferredSize: Size.fromHeight(70.0),
                child: AppBar(
                  title: Text("Post Photos"),
                  actions: [
                    photos.length > 0
                        ? IconButton(
                            icon: Icon(Icons.upload_file),
                            onPressed: () {
                              upload_pre_photos(context);
                            },
                          )
                        : Text(""),
                  ],
                  // backgroundColor: Theme.of(context).primaryColor,
                )),
            floatingActionButton: job_card['wash_status'].toString() == "4" ||
                    job_card['display'] == null
                ? null
                : photos.length == 4
                    ? null
                    : FloatingActionButton(
                        heroTag: "btn",
                        onPressed: () async {
                          final pickedFile =
                              await picker.getImage(source: ImageSource.camera);

                          if (pickedFile != null) {
                            final local_path = File(pickedFile.path);
                            setState(() {
                              get_photo = true;
                            });
                            final appDir = await syspaths
                                .getApplicationDocumentsDirectory();

                            print(appDir.path);

                            final file_path = path.basename(pickedFile.path);

                            final savedFile = await local_path
                                .copy("${appDir.path}/$file_path");
                            final List lists_photos =
                                await StorePhotos.getPhotosOfPostPrefernces(
                                    job_card['wash_id']);
                            Map obj = {
                              "store_file": savedFile.path.toString(),
                              "filename": pickedFile.path.toString()
                            };
                            lists_photos.add(obj);
                            StorePhotos.saveImageOfPostToPreferences(
                                    lists_photos, job_card['wash_id'])
                                .then((value) {
                              if (value) {
                                print("Added successfully");
                                getIMages();
                              }
                            }).catchError((error) {
                              print("error $error");
                            });
                            print(savedFile);
                          }
                        },
                        child: Icon(Icons.add_a_photo)),
            body: isLoading
                ? Loader()
                : images.length > 0
                    ? ListView.builder(
                        itemCount: images.length,
                        itemBuilder: (_, index) {
                          return Card(
                              child: Stack(children: [
                            SizedBox(
                                child: Image.network(
                              images[index],
                              height: 220,
                              fit: BoxFit.cover,
                              width: MediaQuery.of(context).size.width,
                            )),
                          ]));
                        })
                    : photos.length > 0
                        ? ListView.builder(
                            itemCount: photos.length,
                            itemBuilder: (_, index) {
                              return Card(
                                  child: Stack(children: [
                                SizedBox(
                                    child: Image.file(
                                  File(photos[index]['store_file']),
                                  height: 220,
                                  fit: BoxFit.cover,
                                  width: MediaQuery.of(context).size.width,
                                )),
                                Padding(
                                  padding: EdgeInsets.all(5),
                                  child: Align(
                                      alignment: Alignment.topRight,
                                      child: FloatingActionButton(
                                        onPressed: () async {
                                          setState(() {
                                            File(photos[index]['store_file'])
                                                .delete()
                                                .then((value) {
                                              print("$value");
                                            }).catchError((e) => null);
                                            photos.removeAt(index);
                                            StorePhotos
                                                    .saveImageOfPostToPreferences(
                                                        photos,
                                                        job_card['wash_id'])
                                                .then((value) {
                                              if (value) {
                                                print("Deleted successfully");
                                              }
                                            }).catchError((error) {
                                              print("error $error");
                                            });
                                          });
                                        },
                                        child: Icon(Icons.remove),
                                        backgroundColor: Colors.red,
                                        mini: true,
                                        heroTag: "btn$index",
                                      )),
                                )
                              ]));
                            })
                        : NoDataFound()));
  }
}
