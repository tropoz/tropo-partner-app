import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import '../../widgets/AppHeader.dart';

class JobCash extends StatefulWidget {
  JobCash({Key key}) : super(key: key);

  @override
  _JobCashState createState() => _JobCashState();
}

class _JobCashState extends State<JobCash> {

  @override

  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppHeader("101 Job Card"),
      body:Container( 
        height: double.infinity,
        color:HexColor('#5155b1'),
        child: SingleChildScrollView(
          child: Column(
          children:[
             Container(
                color: Colors.white,
                padding: EdgeInsets.all(10),
                child: Container(
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height / 2,
                  padding: EdgeInsets.all(5),  
                  color: HexColor("#58e994"),
                  child:Column( 
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      FlatButton(
                        padding: EdgeInsets.all(20),
                        onPressed: null, child:FloatingActionButton(
                        backgroundColor: HexColor("#51c955"),
                        onPressed: null,
                        child: Icon(Icons.done),
                      )),
                      Text("${'500'.toUpperCase()}",style: TextStyle(fontSize:40,color:Colors.white,fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                      Text("${'Hemant Sekhani to pay in Cash'.toUpperCase()}",style: TextStyle(color:Colors.white,fontWeight: FontWeight.bold,fontSize: 20),textAlign: TextAlign.center,),
                    ],
                  ),
                ),
             ),
              
            Container(
              color: HexColor("#5155b1"),
              padding: EdgeInsets.symmetric(vertical:10,horizontal:20),
              child: Center(
                child: Column( 
                  mainAxisAlignment: MainAxisAlignment.end,
                  children:[
                    SizedBox(
                      height:20,
                    ),
                    Text("ENTER OTP HERE",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 20)),
                    SizedBox(
                      height:20,
                    ),
                    TextFormField(
                      style: TextStyle(color: Colors.white),
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(

                        enabledBorder:UnderlineInputBorder(
                          borderSide:BorderSide(
                            color: Colors.white
                          )
                        ),
                        // fillColor:Colors.white,
                        hintStyle: TextStyle(
                          color: Colors.white,
                        ),
                      
                        hintText: 'Enter otp',
                        labelStyle: TextStyle(
                          color:Colors.white,
                        ),
                      ),
                    ),
                    SizedBox(height:20),
                    Container(
                      padding: EdgeInsets.only(top:6),
                      child: Material(
                      elevation: 5.0,
                      borderRadius: BorderRadius.circular(5.0),
                      color:HexColor("#58e994"),
                      child: MaterialButton(
                          minWidth: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                          // whenever pressed user then click 
                          onPressed: () {
                            Navigator.pop(context);
                          },  
                          child: Text("Cash Collected",style: TextStyle(color: Colors.white),),
                        ),
                      ),
                    )
                  ]
                ),
              ),
            ),
          ]
        ),
        ),
      )
    );
  }
}