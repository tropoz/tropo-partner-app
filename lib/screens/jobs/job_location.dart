import 'package:flutter/material.dart';
import 'package:google_map_polyline/google_map_polyline.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:troppo_mobile/widgets/AppHeader.dart';
import 'package:troppo_mobile/widgets/Loader.dart';

class JobLocation extends StatefulWidget {
  JobLocation({Key key}) : super(key: key);

  @override
  _JobLocationState createState() => _JobLocationState();
}

class _JobLocationState extends State<JobLocation> {
    GoogleMapPolyline googleMapPolyline = GoogleMapPolyline(apiKey: "AIzaSyBi-bkxbmIz0PEda-0f9nDHLZBtMUPCOOI");
    Location location = Location();

    final Set<Polyline> polyline = {};
    List<LatLng> routeCoords = [];
    double my_lat;
    double my_lng;
    _getLocation() async {
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }
    
    location.getLocation().then((value){
      setState(() {
        my_lat = value.latitude;
        my_lng = value.longitude;
      });
    });

  }
  getSomepoints(double lat,double lng) async {
    print("google map polyline");
    routeCoords = await googleMapPolyline.getCoordinatesWithLocation(origin: LatLng(my_lat,my_lng), destination:LatLng(lat,lng), mode: RouteMode.driving);
    if(routeCoords.length > 0){
      setState(() {
          polyline.add(Polyline(
            jointType: JointType.round,
            consumeTapEvents: true,
            polylineId: PolylineId("route1"),
            visible: true,
            points:routeCoords,
            width: 4,
            color:Colors.blue,
        ),);
      });
    }
  }
  @override
  void initState() {
    _getLocation();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
  final Map arguments = ModalRoute.of(context).settings.arguments;

  final double lat = double.parse(arguments['lat'] ?? 0.0.toString());
  final double lng = double.parse(arguments['lng'] ?? 0.0.toString());
  location.onLocationChanged.listen((LocationData currentLocation) {
      my_lat = currentLocation.latitude;
      my_lng = currentLocation.longitude;
  });
    print(arguments);
    return Scaffold(
      appBar:AppHeader("Job Location"),
      body:Stack(
        children:[
          (my_lng != null && my_lat != null)  ? GoogleMap(
            onMapCreated:(GoogleMapController controller){
              getSomepoints(lat,lng);
            },
            markers: Set<Marker>.of(
              <Marker>[
                Marker(
                    markerId: MarkerId("1"),
                    position: LatLng(my_lat, my_lng),
                    icon: BitmapDescriptor.defaultMarker,
                    infoWindow:InfoWindow(
                      title: "${arguments['customer_name']}",
                    ),
                  ),
                Marker(
                    markerId: MarkerId("2"),
                    position: LatLng(lat, lng),
                    icon: BitmapDescriptor.defaultMarker,
                    infoWindow:InfoWindow(
                      title: "${arguments['customer_name']}",
                    ),
                  ) 
              ]
            ),
            polylines: polyline,
            initialCameraPosition:CameraPosition(
              target: LatLng(my_lat,my_lng),
              zoom:17,
              
            )) : Loader(),
        ]
      )
    );
  }
}