import 'dart:convert';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phone_direct_caller/flutter_phone_direct_caller.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:troppo_mobile/constants.dart';
import 'package:troppo_mobile/providers/Auth.dart';
import 'package:troppo_mobile/widgets/Loader.dart';
import 'package:troppo_mobile/widgets/NoDataFound.dart';

import '../../widgets/AppHeader.dart';

class MyJob extends StatefulWidget {
  MyJob({Key key}) : super(key: key);

  @override
  _MyJobState createState() => _MyJobState();
}

class _MyJobState extends State<MyJob> {
  List jobs = [];

  bool isLoading = true;

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();


  Future getJobs() async {
    try{
      final String token = await Provider.of<Auth>(context,listen: false).getToken();
      print(token);
      http.Response res = await http.get("$api_url/api/cleaner/my_jobs",headers: {
        "Authorization":"Bearer $token"
      });
      var res_body = jsonDecode(res.body);
      if(res.statusCode == 200){
        setState(() {
          jobs = res_body['data'];
        });

        print(jobs);
      }
      setState(() {
        isLoading = false;
      });
    }catch(e){
      throw e;
    }
  }
  @override
  void initState() {
    getJobs();

    Future.delayed(Duration.zero,(){
      final Map detail = ModalRoute.of(context).settings.arguments;
      if(detail != null && detail['rating'] != null){
        AwesomeDialog(
            context: context,
            dialogType: DialogType.SUCCES,
            animType: AnimType.BOTTOMSLIDE,
            title: 'Your rating has been submitted',
            desc: 'Thanks for rating,we hope you got excellent service.',
            btnOkColor: HexColor("#5155b1"),
            btnOkOnPress: () {},
        )..show();
      }
      });
    super.initState();
  }

  void _showSnackBar(value,{Color color:Colors.red}) {
    final snackBar = SnackBar(
      content: Text(value),
      backgroundColor: color,
      duration: Duration(seconds: 3),

    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }
  @override
  Widget build(BuildContext context) {
    final user_detail = Provider.of<Auth>(context,listen: false).getUser;
    return WillPopScope(child: Scaffold(
      key: _scaffoldKey,
      appBar:AppHeader("My Job"),
      body: isLoading ? Loader() : jobs.length > 0 ? Container(
        child: RefreshIndicator(child:ListView.builder(itemCount: jobs.length,itemBuilder:(create,index){
            return GestureDetector(
              onTap: (){
                  if(user_detail['data']['working_status'].toString() == "1" || jobs[index]['status'].toString() == "1"){
                    Navigator.pushNamed(context, "/job/card",arguments:{
                      "jobs_data":jobs[index]
                    });
                  }else if(jobs[index]['status'].toString() != "1"){
                      _showSnackBar("You are not in this job");
                  }else{
                      _showSnackBar("You are on another job");

                  }
               },
              child:Padding(
                padding: const EdgeInsets.all(5),
                child: Card(
                elevation: 5,
                shape: jobs[index]['status'].toString() == "1" ? new RoundedRectangleBorder(side: new BorderSide(color:HexColor("#a0e49e"), width: 2.0)) : null,
                child: Column(
                  children: [
                    ListTile(
                      title: Text("Service Name"),
                      subtitle: Text("${jobs[index]['service_name']}"),
                      trailing: Text("${jobs[index]['job_card_id']}",style: TextStyle(color:HexColor('#5155b1')),),
                    ),
                    ListTile(
                      title: Text("Customer Name"),
                      subtitle: Text("${jobs[index]['customer_name']}"),
                      trailing: IconButton(padding:EdgeInsets.only(left:20),icon: Icon(Icons.call,size: 30,color:HexColor('#5155b1')),onPressed:() async {
                        await FlutterPhoneDirectCaller.callNumber("${jobs[index]['customer_mobile']}");
                      },),

                    ),

                    ListTile(
                      title: Text("Address"),
                      subtitle: Text("${jobs[index]['customer_address']}"),
                      trailing: IconButton(padding:EdgeInsets.only(left:20),icon: Icon(Icons.location_on,size: 30,color: HexColor('#5155b1'),),onPressed:(){
                        print(jobs[index]);
                        Navigator.pushNamed(context, "/job/location",arguments:{
                          "lat":jobs[index]['lat'],
                          "lng":jobs[index]['lng'],
                          "customer_name":jobs[index]['customer_name'],

                        });
                      }),

                    ),
                    
                  ],
                  
                ),
            ),
              ));
          }),onRefresh:(getJobs)),
      ) : Center(
        child: NoDataFound(),
      ),
    ), onWillPop:() async {
      final Map detail = ModalRoute.of(context).settings.arguments;
      if(detail != null && detail['rating'] != null){
        Navigator.pushNamedAndRemoveUntil(context, "/home", (route) => false);
      }else{
        return true;
      }
    });
  }
}