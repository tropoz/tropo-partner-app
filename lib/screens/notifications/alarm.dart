// import 'dart:async';

// import 'package:audioplayers/audio_cache.dart';
// import 'package:audioplayers/audioplayers.dart';
// import 'package:flutter/material.dart';
// import 'package:hexcolor/hexcolor.dart';
// import 'package:intl/intl.dart';

// import 'package:swipebuttonflutter/swipebuttonflutter.dart';

// class AlarmPage extends StatefulWidget {
//   AlarmPage({Key key}) : super(key: key);

//   @override
//   _AlarmPageState createState() => _AlarmPageState();
// }

// class _AlarmPageState extends State<AlarmPage> {

//   String _timeString;

//   var timer;

  
//   AudioPlayer player;
//   void startAlarm() async {
//       AudioCache audioPlayer = AudioCache();
//       AudioPlayer p_audio = await audioPlayer.play("sound_alarm.mp3");

//       setState(() {
//         player = p_audio;
//       });
//   }
//   @override
//   void initState() {
//     startAlarm();
//     _timeString = _formatDateTime(DateTime.now());
//     timer = Timer.periodic(Duration(seconds: 1), (Timer t) => _getTime());

//     super.initState();
//   }

//   void _getTime() {
//     final DateTime now = DateTime.now();
//     final String formattedDateTime = _formatDateTime(now);
//     if(mounted){
//       setState(() {
//         _timeString = formattedDateTime;
//       });
//     }
//   }

//   String _formatDateTime(DateTime dateTime) {
//     return DateFormat('hh:mm').format(dateTime);
//   }

//   @override
//   void dispose() {
//     timer.cancel();
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       decoration:BoxDecoration(
//         gradient: LinearGradient(
//           begin: Alignment.topCenter,
//           end:Alignment(0.2, 0.3), // 10% of the width, so there are ten blinds.
//           colors: <HexColor>[
//             HexColor("#29304A"),
//             HexColor("#20263B"),
//           ], // red to yellow
//           tileMode: TileMode.repeated, // repeats the gradient over the canvas
//         ),
//       ),
//        child:Padding(padding:EdgeInsets.only(top:100),child:Column(
//          children:[
//            SizedBox(
//              height: 120,
//              child: Text(_timeString,style: TextStyle(color: Colors.white,fontSize: 50),),
//            ),
//            Icon(Icons.alarm,color:Colors.white,size:250,),

//           SizedBox(
//             height:60
//           ),
//           SwipingButton(text: "Swipe to Turn Off", onSwipeCallback:(){
//             player.stop();
//           },padding: EdgeInsets.all(10),swipeButtonColor: HexColor("#20263B"),iconColor: Colors.white,backgroundColor: HexColor("#20263B"))
           
//          ],
         
//        )),
//     );
//   }
// }