// import 'package:audioplayers/audio_cache.dart';
// import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';

import 'package:troppo_mobile/widgets/NoDataFound.dart';

import '../../widgets/AppHeader.dart';

import 'package:swipe_to/swipe_to.dart';

class NotificationApp extends StatefulWidget {

  @override
  _NotificationAppState createState() => _NotificationAppState();
}

class _NotificationAppState extends State<NotificationApp> {

  List<Map> a = [{"name":1},{"name":2},{"name":3},{"name":4},{"name":5},{"name":6},{"name":7},{"name":8},{"name":9},{"name":10},{"name":11},{"name":12}];

  int count = 0;


  @override
  void initState() {
    a.forEach((element) {
      element['checked'] = false;
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppHeader("My Notification"),
      body:a.length > 0 ?  ListView.separated(
        separatorBuilder: (BuildContext context, int index){
          return Divider();
        },
        itemBuilder: (_,index){
        return SwipeTo(
          child:ListTile(
            title:Text(a[index]['name'].toString()),
            subtitle: Text("2 min ago"),
            onTap: () async {
              // AudioCache audioPlayer = AudioCache();
              // AudioPlayer player = await audioPlayer.play("sound_alarm.mp3");
              // Future.delayed(Duration(seconds:3),(){
              //   player.stop();
              // });
              Navigator.pushNamed(context, "/start/alarm");
            },
          ),
          iconOnLeftSwipe:Icons.delete,
          iconColor: Colors.red,
          onLeftSwipe: (){
            return showDialog<void>(
                context: context,
                
                barrierDismissible: false, // user must tap button!
                builder: (BuildContext context) {
                  return AlertDialog(
                    insetPadding: EdgeInsets.symmetric(horizontal: 0),
                    title: Text('Do you want delete this record ?'),
                    actions: <Widget>[
                      TextButton(
                        child: Text('Ok'),
                        onPressed: () {
                          setState(() {
                            a.removeAt(index);
                          });
                          Navigator.of(context).pop();
                        },
                      ),
                      TextButton(
                        child: Text('Cancel'),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  );
                },
              );
          },
        );
      },itemCount: a.length) : NoDataFound(),
    );
  }
}