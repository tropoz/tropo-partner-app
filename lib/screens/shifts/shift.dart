import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
// ignore: unused_import

import 'package:http/http.dart';
import 'package:provider/provider.dart';
import 'package:troppo_mobile/constants.dart';
import 'package:troppo_mobile/providers/Auth.dart';
import 'package:troppo_mobile/widgets/NetWorkIssue.dart';
import 'package:troppo_mobile/widgets/ShowError.dart';

import '../../widgets/Loader.dart';

import '../../widgets/NoDataFound.dart';

import '../../widgets/ShowError.dart';

class Shift extends StatefulWidget {
  Shift({Key key}) : super(key: key);

  @override
  _ShiftState createState() => _ShiftState();
}

class _ShiftState extends State<Shift> {



  bool isNetwork = true;



  Future getShifts() async {

      try{
            final token = await Provider.of<Auth>(context,listen: false).getToken();
            Response shift = await get("$api_url/api/cleaner/assigned_shift",headers: {
              "Authorization":"Bearer $token"
            });
            var res = jsonDecode(shift.body);
              if(shift.statusCode == 200){
                if(res['data'].length > 0){
                  return res['data'];
                }else{
                  return null;
                }
              }
            }
      catch(e){
        throw e;
      }  
  }

  void isConnectionMethod() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    print(connectivityResult);
    if (connectivityResult == ConnectivityResult.mobile || connectivityResult == ConnectivityResult.wifi) {
        setState(() {
          isNetwork = true;
        });
    } else{
        setState(() {
          isNetwork = false;
        });
    }
  }
  @override
  void initState() {
    isConnectionMethod();
    super.initState();
  
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:PreferredSize(
          preferredSize: Size.fromHeight(70.0),
          child:AppBar(
            title:Text("My Shifts"),
            backgroundColor: Theme.of(context).primaryColor,
          )
        ),
        body: isNetwork ?  FutureBuilder(
          future: getShifts(),
          builder: (BuildContext context, AsyncSnapshot snapshot){
          if(snapshot.connectionState == ConnectionState.done){
              if(snapshot.hasError){
                return ShowError();
              } 
              if(snapshot.hasData){
                return ListView.builder(itemCount:snapshot.data.length,itemBuilder: (_,index){
                  return Card(
                    child:ListTile(
                      contentPadding:EdgeInsets.all(15),
                      leading: Icon(Icons.watch_later,size:35,color:Colors.red),
                      title:Text("${snapshot.data[index]['from']} - ${snapshot.data[index]['to']}"),
                    )
                  );
                });
              }
              else{
                return NoDataFound();
              }
          }else{
            return Loader();
          }
        }) : NetWorkIssue(isConnectionMethod),
    );
  }
}
