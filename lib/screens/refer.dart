import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:troppo_mobile/widgets/AppHeader.dart';

import '../widgets/BottomNavigation.dart';

class Refer extends StatefulWidget{
  Refer({Key key}) : super(key: key);

  @override
  _ReferState createState() => _ReferState();
}

class _ReferState extends State<Refer> {
  final _formKey = GlobalKey<FormState>();
  bool onSubmit = false;

  int groupValue = 1;
  // ignore: non_constant_identifier_names
  var text_input_name = TextEditingController();
  
  @override
  Widget build(BuildContext context) {
    // ignore: non_constant_identifier_names
    return Scaffold(
      appBar:AppHeader("Refer & Earn",false), 
      bottomNavigationBar: BottomNavigation(3),
      body:SingleChildScrollView(
        child: Container(
        padding: EdgeInsets.all(5),
        child: Card(
          elevation: 3,
        child:Padding(
          padding: EdgeInsets.all(10),
          child:Form(
            // ignore: deprecated_member_use
            autovalidate: onSubmit ? true : false,
            key:_formKey ,
            child: Column(
            children:[
              TextFormField(
                controller: text_input_name,

                decoration: InputDecoration(
                  labelText: 'Name'
                ),
                // ignore: missing_return
                validator: (value){
                  if(value.isEmpty){
                    return "Please enter name";
                  }
                  return null;
                },
              ),
              TextFormField(
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  labelText: 'Mobile Number'
                ),
                validator: (value){
                  if(value.isEmpty){
                    return "Please enter mobile no";
                  }else if(value.length < 10){
                    return "Please enter valid mobile no";
                  }
                  return null;
                }
              ),
              SizedBox(height:17),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Row(
                    children:[
                      Radio(
                        activeColor:HexColor("#3C4CA1"),
                        value: 1, groupValue: groupValue, onChanged: (value){
                        setState(() {
                          groupValue = value;
                        });
                      }),
                      Text("Partner",style: TextStyle(fontWeight: FontWeight.bold),),

                    ]
                  ),
                  Row(
                    children:[
                      Radio(
                        activeColor:HexColor("#3C4CA1"),
                        value: 2, groupValue: groupValue, onChanged: (value){
                        
                        setState(() {
                          groupValue = value;
                        });
                      }),
                      Text("Tropo Customer",style: TextStyle(fontWeight: FontWeight.bold),),
                    ]
                  )
                ],
              ),
              SizedBox(height:17),
                Material(
                elevation: 5.0,
                borderRadius: BorderRadius.circular(5.0),
                color: Theme.of(context).primaryColor,
                child: MaterialButton(
                minWidth: MediaQuery.of(context).size.width,
                padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                onPressed: () {
                  setState(() {
                    onSubmit = true;
                  });
                  if(_formKey.currentState.validate()){
                      print(text_input_name.text);
                      //clean input after submit form 

                      setState(() {
                        onSubmit = false;
                      });

                      FocusScope.of(context).requestFocus(FocusNode());

                  }
                },
                child: Text("Submit",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold)),
                ),
              )
            ],
          ),
          )
        ),
      ),
      ),
      )
    );
  }
}