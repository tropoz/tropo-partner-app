
import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

class JobModel{

  static Future fetch({@required String url,@required String token}) async {
    print(token);
    try{
      http.Response resBody = await http.get(url,headers: {
        "Accept":"application/json",
        "Authorization":"Bearer $token"
      });
      var response = jsonDecode(resBody.body);
      if(resBody.statusCode != 200){
        throw "Error";
      }
      return response;
    }
    catch(e){
      print(e);
      throw e;
    }
  }
}