// import 'package:android_alarm_manager/android_alarm_manager.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';


import 'package:hexcolor/hexcolor.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:troppo_mobile/providers/JobCardProvider.dart';

import 'package:troppo_mobile/screens/authentication/login.dart';

import 'package:flutter/services.dart';

import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:troppo_mobile/screens/jobs/job_location.dart';
import 'package:troppo_mobile/screens/profile/password_reset.dart';
import 'package:troppo_mobile/screens/qr_code_scanner.dart';
import 'package:troppo_mobile/widgets/CustomErrorWidget.dart';

import 'screens/clutters/cluster.dart';

import 'screens/clutters/_cluster.dart';

import 'screens/authentication/home.dart';

import 'screens/authentication/reset_password.dart';

import 'screens/notifications/NotificationApp.dart';

import 'screens/shifts/shift.dart';

import './screens/refer.dart';

import './screens/authentication/create_password.dart';

import './screens/kits/my_kits.dart';

import './screens/profile/profile.dart';

import './screens/profile/personaldetail.dart';

import 'screens/profile/docu_detail.dart';

import 'screens/profile/bank_detail.dart';

import 'screens/profile/refer_earn.dart';

import './screens/profile/settings.dart';

import './screens/profile/legal_info.dart';

import './screens/jobs/pre_photo.dart';

import './screens/profile/privacy_policy.dart';

import './screens/accouts/account.dart';

import 'screens/accouts/cash_collect_today.dart';

import './screens/accouts/cash_collect_month.dart';

import './screens/accouts/cash_deposit.dart';

import './screens/accouts/remuneration_receviced.dart';

import './screens/jobs/job_complete.dart';

import './screens/jobs/job_card.dart';

import './screens/jobs/cash_job.dart';

import 'screens/jobs/my_job.dart';

import './screens/jobs/completed_job_list.dart';

import './screens/authentication/otp_sent.dart';

import 'package:provider/provider.dart';

import './providers/Auth.dart';

import './providers/ForgotPasswordProvider.dart';
    
import './screens/profile/profile_update.dart';

import './screens/jobs/post_photo.dart';


import './screens/notifications/alarm.dart';

Future myBackgroundService(message) async {
    await Firebase.initializeApp();
    print("Background service is running");
}

 Future notificationSelected(String payload) async {
  print("Okk");
}

printHello()  async {
  // FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  // _firebaseMessaging.configure(
  //   onMessage: myBackgroundService,
  // );
  // final DateTime now = DateTime.now();



  // FlutterLocalNotificationsPlugin fltrNotification;

  // int count_notification = 1;
  // var androidInitilize = new AndroidInitializationSettings('@mipmap/ic_launcher');
  // var iOSinitilize = new IOSInitializationSettings();
  // var initilizationsSettings = new InitializationSettings(android:androidInitilize,iOS:iOSinitilize);
  // fltrNotification = new FlutterLocalNotificationsPlugin();
  // fltrNotification.initialize(initilizationsSettings,
  //     onSelectNotification: notificationSelected);
  // var androidDetails = new AndroidNotificationDetails(
  //       "Channel ID", "Desi programmer", "This is my channel",
  //       importance: Importance.max,playSound: true,sound: RawResourceAndroidNotificationSound('notification'));
  // var iSODetails = new IOSNotificationDetails();
  // var generalNotificationDetails =
  //       new NotificationDetails(android:androidDetails,iOS:iSODetails);
  // await fltrNotification.show(count_notification, "title"," body", generalNotificationDetails,payload: 'Default_Sound');
  // count_notification++;




  // print("[$now] Hello, world! isolate= function='$printHello'");

}


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  // final int helloAlarmID = 0;
  // await AndroidAlarmManager.initialize();

  // FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  // _firebaseMessaging.configure(
  //   onBackgroundMessage:myBackgroundService,
  // );

  SharedPreferences prefs = await SharedPreferences.getInstance();                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
  var token = prefs.getString("token");
  // await prefs.remove("token");
  // protrait mode only in mobile
  SystemChrome.setPreferredOrientations ([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown
  ]);

  runApp(MultiProvider(
    
    child: MaterialApp(
      // debugShowCheckedModeBanner: false,
      builder:(BuildContext context, Widget widget) {
        ErrorWidget.builder = (FlutterErrorDetails errorDetails) {
          return CustomErrorWidget(errorDetails: errorDetails);
        };
        EasyLoading.init();
        return widget;
      },
      title: 'Tropo Partner',
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
        // This is the theme of your application.        
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primaryColor:HexColor('#5155b1'),
        backgroundColor: HexColor("#f1f0f1"),
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms
        // routes handles here
      ),
      initialRoute: '/',
      routes:{
        "/": (ctx)=> token != null ? Home() : Login(),
        "/login": (ctx)=> Login(),
        "/home": (ctx)=> Home(),
        "/forget/password": (ctx)=> ResetPassword(),
        "/otp/sent": (ctx)=> OTPSent(),
        "/cluster":(ctx)=>Cluster(),
        "/cluster/location":(ctx)=>LocationCluster(),
        "/shift":(ctx)=>Shift(),
        "/kits":(ctx)=>MyKit(),
        '/refer':(ctx)=>Refer(),
        '/profile':(ctx)=>Profile(),
        '/profile/detail':(ctx)=>PersonalDetail(),
        '/profile/update':(ctx)=>ProfileUpdate(),
        '/profile/document':(ctx)=>DocumentDetail(),
        '/profile/bank/detail':(ctx)=>BankDetail(),
        '/profile/refer_earn':(ctx)=>ReferEarn(),
        '/profile/settings':(ctx)=>Setting(),
        '/profile/legal_info':(ctx)=>LegalInfo(),
        '/profile/privacy_policy':(ctx)=>PrivacyPolicy(),
        '/account':(ctx)=>Account(),
        '/account/cash/collect':(ctx)=>CashCollect(),
        '/account/cash/month':(ctx)=>CashCollectMonth(),
        '/account/cash/deposit':(ctx)=>CashDeposit(),
        '/account/cash/remuneration_receviced':(ctx)=>RemunationRecieve(),
        '/job/complete':(ctx)=>JobComplete(),
        '/my_job':(ctx)=>MyJob(),
        '/job/cash':(ctx)=>JobCash(),
        '/job/card':(ctx)=>JobCard(),
        "/job/location":(ctx)=>JobLocation(),
        '/job/completed/list':(ctx)=>JobCompletedList(),
        '/notifications':(ctx)=>NotificationApp(),
        '/jobs/pre-photo':(ctx)=>PrePhoto(),
        '/jobs/post-photo':(ctx)=>PostPhoto(),
        "/create/password":(ctx)=>CreatePassword(),
        "/reset/password":(ctx)=>ResetPasswordWithOld(),
        "/qr_code/scanner":(ctx)=>QRViewExample(),
        // "/start/alarm":(ctx)=>AlarmPage()

      },
      
    ),
    providers:[
      ChangeNotifierProvider(create:(_)=>Auth()),
      ChangeNotifierProvider(create:(_)=>ForgotPasswordProvider()),
      ChangeNotifierProvider(create:(_)=>JobCardProvider()),
    ]
  ));

  //  await AndroidAlarmManager.oneShot(Duration(seconds: 1),helloAlarmID,printHello,wakeup: true);
}