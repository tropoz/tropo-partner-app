import 'dart:io';

import 'package:flutter/material.dart';

void SocketError(BuildContext context){
  showDialog(
          barrierDismissible: false,
          context: context, 
          child:new AlertDialog(
          title: Row(
            children: [
              Icon(Icons.signal_cellular_alt,color: Colors.red,),
              new Text("Something went wrong"),
            ],
          ),
          content: Text("Please try again later"),
          actions: [
          new FlatButton(
                child: const Text("Ok"),
                onPressed: (){
                  Navigator.pop(context, true); 
                  // if error occur then exit from app
                  
                  // exit(0);
                },
            ),
          ],
        )
      );
}