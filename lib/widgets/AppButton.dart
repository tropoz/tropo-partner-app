import 'package:flutter/material.dart';

class AppButton extends StatelessWidget {
  Function method;
  String text;
  double  fontSize = 20;
  double minWidth;
  AppButton({
    @required this.text,
    @required this.method,
    this.fontSize,
    this.minWidth
  });

  @override
  Widget build(BuildContext context) {
    return Material(
        elevation: 5.0,
        borderRadius: BorderRadius.circular(5.0),
        color: Theme.of(context).primaryColor,
        child: MaterialButton(
          minWidth: minWidth != null ? minWidth : MediaQuery.of(context).size.width,
          padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          onPressed:method,
          child: Text(
            text,
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white, fontSize:fontSize),
          ),
        ),
      );
  }
}