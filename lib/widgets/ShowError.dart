import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';


class ShowError extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return  Center(
        child:Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
          Icon(Icons.error,size: 80,color:Colors.red),
           Text("Error occured!",style:TextStyle(color: HexColor("#3C4CA1"),fontSize: 20),),
          ],),
      );
  }
}