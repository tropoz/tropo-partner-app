import 'package:flutter/material.dart';

// ignore: must_be_immutable
class AppHeader extends StatelessWidget with PreferredSizeWidget {
  String title = "";
  bool headerShow = true;
  AppHeader(this.title,[this.headerShow=true]);
  @override
  Widget build(BuildContext context) {
    return PreferredSize(
         preferredSize: Size.fromHeight(70.0),
         child:AppBar(
          automaticallyImplyLeading: headerShow,
          title:Text("$title"),
          // backgroundColor: Theme.of(context).primaryColor,
       )
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(70.0);
}