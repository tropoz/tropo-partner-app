import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

import './NoDataFound.dart';
class AppListView extends StatelessWidget {
  final int itemCount;
  final itemBuilder;

  AppListView(this.itemCount,this.itemBuilder);

  @override
  Widget build(BuildContext context) {
    return Padding(padding:EdgeInsets.only(top:10),
    child: itemBuilder.length > 0 ? (ListView.builder(
      itemCount: this.itemCount,
      itemBuilder: (context,index){
        return Card(elevation:2,child:ListTile(
          leading: Icon(Icons.location_on,size: 45,color:HexColor('#d3e153')),
          subtitle: Row(children: [
            Icon(Icons.call_made,color: Colors.red),
            Text("${itemBuilder[index]['city']},${itemBuilder[index]['state']}"),
          ],),
          contentPadding:EdgeInsets.all(15),
          onTap:(){
            Navigator.pushNamed(context, "/cluster/location",arguments: {"cluster":itemBuilder[index]});
          },
          title:Text(
          itemBuilder[index]['name']
          )));
        },
      )) : NoDataFound()
    );
  }
}