import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class ServerErrorWidget extends StatelessWidget {
  bool isError;
  Widget contain;
  ServerErrorWidget({this.isError,this.contain});

  @override
  Widget build(BuildContext context) {
    return isError ? Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children:[
              Icon(
                Icons.dns,
                color:HexColor("5155b1"),
                size: 60,
              ),
              Text("Couldn't connect",style: TextStyle(fontSize:20),),
              Text("There may be a problem with the server.",),
              Text("Please try again later."),

            ]
          ),
        ),
    ) : contain;
  }
}