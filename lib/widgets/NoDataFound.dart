import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';


class NoDataFound extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return  Center(
        child:Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
          Icon(Icons.find_in_page,size: 80,color: HexColor("##3C4CA1")),
           Text("OOOPS!",style:TextStyle(color: HexColor("#3C4CA1"),fontSize: 20),),
           Text("No data found yet.",style: TextStyle(color:HexColor('#93959F')),)
        ],),
      );
  }
}