import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:troppo_mobile/screens/accouts/account.dart';
import 'package:troppo_mobile/screens/authentication/home.dart';
import 'package:troppo_mobile/screens/jobs/completed_job_list.dart';
import 'package:troppo_mobile/screens/partner.dart';
import 'package:troppo_mobile/screens/refer.dart';

class BottomNavigation extends StatefulWidget {
  int index;
  BottomNavigation(this.index);

  @override
  _BottomNavigationState createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation> {
  int _selectedIndex = 0;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
    if(index == 0){
      Future.delayed(Duration.zero,(){
         Navigator.pushReplacement(
              context, 
              PageRouteBuilder(
                pageBuilder: (context, animation1, animation2) => Home(),
                transitionDuration: Duration(seconds: 0),
            ),
        );
      });
    }else if(index == 1){
        Future.delayed(Duration.zero,(){
        Navigator.pushReplacement(
              context, 
              PageRouteBuilder(
                pageBuilder: (context, animation1, animation2) => Account(),
                transitionDuration: Duration(seconds: 0),
            ),
        );

      });
    }
    else if(index == 2){
        Future.delayed(Duration.zero,(){
        Navigator.pushReplacement(
              context, 
              PageRouteBuilder(
                pageBuilder: (context, animation1, animation2) => JobCompletedList(),
                transitionDuration: Duration(seconds: 0),
            ),
        );

      });
    }
    else if(index == 3){
        Future.delayed(Duration.zero,(){
        Navigator.pushReplacement(
              context, 
              PageRouteBuilder(
                pageBuilder: (context, animation1, animation2) => Refer(),
                transitionDuration: Duration(seconds: 0),
            ),
        );

      });
    }
    else if(index == 4){
        Future.delayed(Duration.zero,(){
        Navigator.pushReplacement(
              context, 
             
              PageRouteBuilder(
                pageBuilder: (context, animation1, animation2) => PartnerCare(),
                transitionDuration: Duration(seconds: 0),
              
            ),
        );

      });
    }
  }
  @override
  Widget build(BuildContext context) {
    _selectedIndex = widget.index;
    return BottomNavigationBar(
       type: BottomNavigationBarType.fixed, 
       iconSize: 27,
        backgroundColor: HexColor("#5155B1"),
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            label: 'Account',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.check_circle),
            label: 'Completed jobs',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.attach_money),
            label: 'Refer',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.support_agent),
            label: 'PC',
          ),
        ],
         currentIndex: _selectedIndex,
        selectedItemColor: Colors.white,
        onTap: _onItemTapped,
      );
  }
}