import 'package:flutter/material.dart';

class NetWorkIssue extends StatelessWidget {
  
  final isConnectionMethod;
  const NetWorkIssue(this.isConnectionMethod);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Center(
        child:Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(Icons.signal_cellular_alt,size:60),
            FlatButton(onPressed:isConnectionMethod,textColor: Colors.blue,child:Text("Try Again"))
          ],
        ),
      )
    );
  }
}