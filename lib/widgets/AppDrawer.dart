import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:provider/provider.dart';
import '../providers/Auth.dart';

class AppDrawer extends StatelessWidget {

  @override
  Widget build(BuildContext context) {

    // ignore: non_constant_identifier_names
    final user_intance = Provider.of<Auth>(context,listen: false);

    var name = "";
    var email = "";
    var avatar = "";

    if(user_intance.getUser != null){
      name = user_intance.getUser['data']['name'];
      email = user_intance.getUser['data']['email'];
      avatar = user_intance.getUser['data']['avatar'];
    }

    return Drawer(
        child:SingleChildScrollView(
          child:  Column(
          children:[
            Container(
              width:double.infinity,
              padding:EdgeInsets.all(20),
              color:Theme.of(context).primaryColor,
              child:Center(
                child:Column(
                  children:[
                    avatar.isNotEmpty ? Container(
                      width:100,
                      height:100,
                      margin:EdgeInsets.only(top:30),
                      decoration:BoxDecoration(
                        borderRadius: BorderRadius.circular(50)
                      ),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(50),
                        child: FadeInImage.assetNetwork(  
                            placeholder: 'assets/pic.png',
                            image:"$avatar"
                        ),
                      )
                    ) : Container(
                      width:100,
                      height:100,
                      margin:EdgeInsets.only(top:30),
                      child:ClipRRect(
                        borderRadius: BorderRadius.circular(50),
                        child: Image.asset("assets/pic.png"),
                      )
                    ),
                    Text('$name',style: TextStyle(fontSize: 20,color: Colors.white)),
                    Text('$email',style: TextStyle(color: Colors.white)),
                  ]
                )
              )
            ),
            ListTile(
              leading:Icon(Icons.move_to_inbox),
              title:Text('DUTY',style:TextStyle(fontSize:18,color:HexColor('#686c78'))),
              onTap:null
            ),
            Divider(),
            ListTile(
              leading:Icon(Icons.move_to_inbox),
              title:Text('My Kits',style:TextStyle(fontSize:18,color:HexColor('#686c78'))),
              onTap:(){
                Navigator.pop(context);
                Navigator.pushNamed(context,"/kits");
              }
            ),
            Divider(),

            ListTile(
              leading:Icon(Icons.delete),
              title:Text('My Jobs',style:TextStyle(fontSize:18,color:HexColor('#686c78'))),
              onTap:(){
                Navigator.pop(context);
                Navigator.pushNamed(context, "/my_job");
              }
            ),
            Divider(),

            ListTile(
              leading:Icon(Icons.library_books),
              title:Text('My Cluters',style:TextStyle(fontSize:18,color:HexColor('#686c78'))),
              onTap:(){
                Navigator.pop(context);
                Navigator.pushNamed(context,"/cluster");
              }
            ),
            Divider(),

            ListTile(
              leading:Icon(Icons.cloud_sharp),
              title:Text('My Shifts',style:TextStyle(fontSize:18,color:HexColor('#686c78'))),
              onTap:(){
                Navigator.pop(context);
                Navigator.pushNamed(context,"/shift");
              }
            ),
            Divider(),

            ListTile(
              leading:Icon(Icons.report),
              title:Text('Profile',style:TextStyle(fontSize:18,color:HexColor('#686c78'))),
              onTap:(){
                Navigator.pop(context);
                Navigator.pushNamed(context, "/profile"); 
              }
            ),
            Divider(),
            ListTile(
              leading:Icon(Icons.flag),
              title:Text('Exit Parter App',style:TextStyle(fontSize:18,color:HexColor('#686c78'))),
              onTap:(){
                user_intance.logout().then((v){
                    Navigator.pushNamedAndRemoveUntil(context, "/login",(_)=>false);
                });
              }
            ),
          ]),
        ),
      );
  }
}