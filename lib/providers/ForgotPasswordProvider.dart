import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;

import '../constants.dart';

class ForgotPasswordProvider extends ChangeNotifier{
  String otp = '';

  String get getOTP => otp;


  Future otpSend(mobile) async {
    try{
      http.Response sendOtp = await http.post(
        "$api_url/api/cleaner/forgot_password",
        body: {
          'mobile':mobile
        }
      );

      if(sendOtp.statusCode == 200){
        otp = json.decode(sendOtp.body)['otp'];        
      } 

      notifyListeners();
      return sendOtp;
    }
    catch(e){
      throw e;
    }
  }
}