import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'package:http/http.dart' as http;

import '../constants.dart';

// ignore: must_be_immutable
class Auth extends ChangeNotifier {
  Map user;

  SharedPreferences prefs;

  bool isLoading = true;

  Future isAuthenticated() async {
    prefs = await SharedPreferences.getInstance();
    var isAuth = prefs.getString("token") ?? false;
    if (isAuth) {
      return true;
    }

    return false;
  }

  Future setToken(String token) async {
    prefs = await SharedPreferences.getInstance();
    await prefs.setString("token", token);
  }

  Future getUserApi() async {
    var token = await getToken();
    print(token);
    if (token != null) {
      // ignore: non_constant_identifier_names
      try {
        // ignore: non_constant_identifier_names
        // http call for user details
        http.Response user_detail =
            await http.post("$api_url/api/cleaner/user_detail", headers: {
          "Accept": "application/json",
          "Authorization": "Bearer $token",
        });
        this.user = json.decode(user_detail.body);

        this.user['status_code'] = user_detail.statusCode;

        if (this.user.length != 0) {
          this.isLoading = false;
        }

        print(this.isLoading);
      } catch (e) {
        throw e;
      }
    }
    notifyListeners();
  }

  Future getToken() async {
    prefs = await SharedPreferences.getInstance();
    var userToken = prefs.getString("token");
    return userToken;
  }

  Map get getUser => user;

  Future<http.Response> authenticate(Map authParams) async {
    try {
      http.Response authRes =
          await http.post("$api_url/api/cleaner/login", headers: {
        "Accept": "application/json; charset=UTF-8",
      }, body: {
        'mobile': authParams['mobile'],
        'password': authParams['password']
      });
      print("Happens");

      if (authRes.statusCode == 200) {
        var authData = json.decode(authRes.body);
        setToken(authData['data']['token']);
      }
      return authRes;
    } catch (e) {
      print("Error occured");
      throw e;
    }
  }

  Future logout() async {
    try{
      prefs = await SharedPreferences.getInstance();
      await prefs.remove("token");
      
    }catch(e){
      throw e;
    }
  }
}