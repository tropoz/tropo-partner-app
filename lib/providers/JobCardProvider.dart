import 'package:flutter/material.dart';

class JobCardProvider extends ChangeNotifier {

    bool isEndWash = false;

    bool isCompletedJob = false;

    void setEndWash() {
      this.isEndWash = true;
      notifyListeners();
    }

    void setCompletedJob() {
      this.isCompletedJob = true;
      notifyListeners();
    }
}